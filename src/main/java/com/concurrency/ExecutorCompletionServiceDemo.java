package com.concurrency;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;

//I-Callable<T> (with return:  V call()) vs I-Runnable (no return)

// https://dzone.com/articles/executorservice-vs
// Using ExecutorCompletionService, submit() Callable task, get return as Future (get())
public class ExecutorCompletionServiceDemo implements Callable<String> {

	int min = 5;
	int max = 15;
	private static boolean add;

	@Override
	public String call() throws Exception {
		int milliseconds = ThreadLocalRandom.current().nextInt(min, max + 1) * 1000;
		System.out.println(Thread.currentThread().getName()
				+ " -- is doing Long running Business process that will take x seconds: " + milliseconds / 1000);
		longRunningTask(milliseconds);
		System.out.println(Thread.currentThread().getName() + " -- Complete!");
		// return the thread name executing this callable task
		return Thread.currentThread().getName() + "-RESULT";
	}

	// Mimic Long running process
	public void longRunningTask(int milliseconds) throws InterruptedException {
		Thread.currentThread();
		Thread.sleep((long) milliseconds);
	}

	public static void main(String args[]) {
		// 1 - Get ExecutorService from Executors utility class, thread pool
		// size is 10
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		CompletionService<String> executorCompletionService = new ExecutorCompletionService<>(executorService);

		// 2 - create a list to hold the Future object associated with Callable
		List<Future<String>> list = new ArrayList<Future<String>>();

		// 3- Create MyCallable instance
		Callable<String> callableTask = new CallableTaskJob();

		// 4- submit to Kick off task process, get Future<String> as return stub
		System.out.println(new Date() + " --- Set up Future tasks");
		for (int i = 0; i < 10; i++) {
			// add Future to the list, we can get return value using Future
			add = list.add(executorCompletionService.submit((Callable<String>) callableTask));
		}

		// 5- wait for all Future<String> results (get())
		System.out.println(new Date() + " --- Start to Get Future results");
        for(Future<String> future : list){
		try {
			// No output delay, as each task complete
			System.out.println(new Date() + " -- Got Future result - " + "::" + executorCompletionService.take().get()); // get(longtimeout,TimeUnitunit)
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
        }

		System.out.println("Finish of Loop");

		// 6 - shut down the executor service now
		executorService.shutdown();
		System.out.println("Shutdown.");
	}
	
	
	void solve(Executor e, Collection<Callable<Result>> solvers)  throws InterruptedException {
		        CompletionService<Result> ecs = new ExecutorCompletionService<Result>(e);
		        int n = solvers.size();
		        List<Future<Result>> futures = new ArrayList<Future<Result>>(n);
		        Result result = null;
		        try {
		            for (Callable<Result> s : solvers)
		                futures.add(ecs.submit(s));
		            for (int i = 0; i < n; ++i) {
		                try {
		                    Result r = ecs.take().get();
		                    if (r != null) {
		                        result = r;
		                        break;
		                    }
		                } catch(ExecutionException ignore) {}
		            }
		        }
		        finally {
		            for (Future<Result> f : futures)
		                f.cancel(true);
		        }
		 
		        if (result != null)
		            use(result);
		    }

}
package com.concurrency;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;


//I-Callable<T> (with return:  V call()) vs I-Runnable (no return)

// Using ExecutorService, submit() Callable task, get return as Future (get())
public class CallableTaskJob implements Callable<String> {

	int min=5;
	int max=15;

    @Override
    public String call() throws Exception {    
        int milliseconds = ThreadLocalRandom.current().nextInt(min, max + 1) * 1000;        
        System.out.println(Thread.currentThread().getName() + " -- is doing Long running process that will take x seconds: " + milliseconds/1000);
        longRunningTask(milliseconds);
        System.out.println(Thread.currentThread().getName() + " -- Complete!");
        //return the thread name executing this callable task
        return Thread.currentThread().getName()+"-RESULT";
    }
    
    //Mimic Long running process
    public void longRunningTask(int milliseconds) throws InterruptedException{
        Thread.currentThread();
		Thread.sleep((long) milliseconds );    	
    }
    
    public static void main(String args[]){
        //1 - Get ExecutorService from Executors utility class, thread pool size is 10
        ExecutorService executorSrv = Executors.newFixedThreadPool(10);

        //2 - create a list to hold the Future object associated with Callable
        List<Future<String>> list = new ArrayList<Future<String>>();
        
        //3- Create MyCallable instance
        Callable<String> callableTask = new CallableTaskJob();

        //4- submit to Kick off task process, get Future<String> as return stub  
        System.out.println(new Date() + " --- Set up Future tasks");
        for(int i=0; i< 10; i++){
            //submit Callable tasks to be executed by thread pool
            Future<String> future = executorSrv.submit(callableTask);   // Callable.call()
            
            //add Future to the list, we can get return value using Future
            list.add(future);
        }
                
        // 5- wait for all Future<String> results (get())
        System.out.println(new Date() + " --- Start to Get Future results");
        for(Future<String> future : list){
            try {
                //print the return value of Future, notice the output delay until thread-1 is completed
            	System.out.println(new Date()+ " -- Got Future result - " +  "::"+future.get());  //get(longtimeout,TimeUnitunit)
            	// process result fut.get(), e.g.
            	// totsum = totsum+ fut.get();            	
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        
        System.out.println("Out of Loop");
        
        //6 - shut down the executor service now
        executorSrv.shutdown();
        System.out.println("Shutdown");
    }

}
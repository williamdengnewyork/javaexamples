package com.concurrency;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

/* Java 8 - leverage Multi-Core in Parallel processing
 	http://www.ryanhmckenna.com/2014/12/multi-core-programming-with-java.html
 */

public class MultiCoreParralell {

	static double montecarlo(int threads, int trials) throws InterruptedException {
		  ExecutorService exec = Executors.newFixedThreadPool(threads);
		  CountDownLatch latch = new CountDownLatch(threads);
		  ComputeUnit[] runnables = new ComputeUnit[threads];

		  Arrays.setAll(runnables, n -> new ComputeUnit(trials));
		  for(ComputeUnit r : runnables) {
		    r.setLatch(latch);
		    exec.execute(r);
		  }

		  latch.await();
		  exec.shutdown();

		  int success = Arrays.stream(runnables).mapToInt(r -> r.getSuccess()).sum();
		  int total = threads * trials;
		  double pi = 4.0 * success / total;
		  return pi;
		}
	
	public static void main(String[] args) throws InterruptedException {
		  System.out.println(montecarlo(8,1000000));
		}  

	
}


class ComputeUnit implements Runnable {
	 
	  private int trials, success;
	  private CountDownLatch latch;

	  public ComputeUnit(int trials) {
	    this.trials = trials;
	    this.success = 0;
	  }

	  public int getTrials() {
	    return trials;
	  }

	  public int getSuccess() {
	    return success;
	  }

	  public void setLatch(CountDownLatch latch) {
	    this.latch = latch;
	  }

	  @Override
	  public void run() {
	    for(int i=0; i < trials; i++) {
	      double x = ThreadLocalRandom.current().nextDouble();
	      double y = ThreadLocalRandom.current().nextDouble();
	      if(x*x + y*y < 1)
	        success++;
	    }
	    latch.countDown();
	  }
	}
package com.algorithm.geek;

//http://www.geeksforgeeks.org/write-a-c-program-to-calculate-size-of-a-tree/

// Time complexity is O(height^2).

public class FindSizeOfTree {

	// A recursive Java program to calculate the size of the tree

    	/* Class to find size of Binary Tree */
		Node root;

		/*
		 * Given a binary tree. Print its nodes in level order using array for
		 * implementing queue
		 */
		int size() {
			return size(root);
		}

		/* computes number of nodes in tree */
		// *****  self recursive self call: size(node) ! Amazing adding up BY ITSELF
		int size(Node node) {
			if (node == null)
				return 0;
			else{
				// This is the magic -- Size of Tree = (size(node.left) + 1 + size(node.right))
				int size_leftnote = size(node.left);   // *****  self recursive self call: size(node) ! Amazing adding up BY ITSELF
				int size_rightnote = size(node.right); //*****  self recursive self call: size(node) ! Amazing adding up BY ITSELF
			
				System.out.println("size(node.left) = "  + size_leftnote);
				System.out.println("size(node.right) = " + size_rightnote);
				return (size_leftnote + 1 + size_rightnote);
			}
		}
		// *****  self recursive self call: size(node) ! Amazing adding up BY ITSELF
		
		
		public static void main(String args[]) {
			
			/* creating a binary tree and entering the nodes */
			FindSizeOfTree tree = new FindSizeOfTree();
			tree.root = new Node(1);
			tree.root.left = new Node(2);
			tree.root.right = new Node(3);
			tree.root.left.left = new Node(4);
			tree.root.left.right = new Node(5);

			System.out.println("The size of binary tree is : " + tree.size(tree.root));
		}
	}


/*
 * Class containing left and right child of current node and key value
 */
class Node {   //binary tree
	int data;
	Node left;
	Node right;

	public Node(int item) {
		data = item;
		left = right = null;
	}
}

package com.algorithm.codility;

import java.util.Arrays;
import java.util.Random;

public class Solution3 {

    public static int getMaxElement(int[] inputArray){ 
	      int maxValue = inputArray[0]; 
	      for(int i=1;i < inputArray.length;i++){ 
	        if(inputArray[i] > maxValue){ 
	           maxValue = inputArray[i]; 
	        } 
	      } 
	      return maxValue; 
	    }
    
    public static int Solution(int[] A){
    	int size=A.length;
    	int a1[]={};
    	int b1[]={}; 
    	int max_a=Integer.MIN_VALUE;
    	int max_b=Integer.MIN_VALUE;
    	int max_diff=0;
    	int abs = 0;
    	//System.out.println(">>> The array" + Arrays.toString(src));
    	
    	int c[]= new int[A.length]; 
    	System.arraycopy(A, 0, c, 0, A.length );
    	Arrays.sort(c);
    	int max_hi=c[c.length-1];
    	//System.out.println("The Max Hi = " + max_hi);

    	
    	for (int i = 0; i<size-1; i++){
    		//System.out.println(i);
    		    		
            a1 = Arrays.copyOfRange(A, 0, i+1);
    		b1 = Arrays.copyOfRange(A, i+1, size);
    		
    		//System.out.println(Arrays.toString(a1));				
    		max_a = getMaxElement(a1);
    		//System.out.println("MaxA Value is: "+max_a);
    		
       		//System.out.println(Arrays.toString(b1));
    		if (max_a < max_hi){
    			max_b = max_hi ;
    		}else{
    			max_b = getMaxElement(b1);	
    		}
       		
       		//System.out.println("MaxB Value is: "+max_b);    			
    		
    		max_diff = Math.abs(max_a - max_b);
    		if (max_diff  > abs){
    		    abs=max_diff;
    		}
    	    //System.out.println("ABS Value is: "+ max_diff);   
    	}
    	return abs;
    }
    
    public static int Solution0(int[] A){
    	int size=A.length;
    	int a1[]={};
    	int b1[]={}; 
    	int max_a=Integer.MIN_VALUE;
    	int max_b=Integer.MIN_VALUE;
    	int max_diff=0;
    	int abs = 0;
    	System.out.println(">>> The array" + Arrays.toString(A));
    	
    	for (int i = 0; i<size-1; i++){
    		//System.out.println(i);
    		    		
            a1 = Arrays.copyOfRange(A, 0, i+1);
    		b1 = Arrays.copyOfRange(A, i+1, size);
    		
    		//System.out.println(Arrays.toString(a1));				
    		max_a = getMaxElement(a1);
    		//System.out.println("MaxA Value is: "+max_a);
    		
   			max_b = getMaxElement(b1);	
       		//System.out.println("MaxB Value is: "+max_b);    			
    		
    		max_diff = Math.abs(max_a - max_b);
    		if (max_diff  > abs){
    		    abs=max_diff;
    		}
    	    //System.out.println("ABS Value is: "+ max_diff);   
    	}
    	return abs;
    }
    
	public static void main(String[] args) {
	
	for (int k=1; k<2; k++){
	System.out.println(k);	
	int src[] = new int[7];
    Random random = new Random();
   // int min=-1000000000;
   // int max=1000000000;
    int min=1;
    int max=10;
    for (int i = 0; i < src.length; i++) {
      src[i] = random.nextInt((max - min) + 1) + min; 
    }

    //int[] src = new int[] {1,3,-3};  // ->6
	//int[] src = new int[] {4, 3, 2, 5, 1,1}; // -> 4
	//int[] src = new int[] {4, 3, 7, 2, 5,6 , 1,1, 9,13}; // -> 9
	//int[] src = new int[] {4, 3, 7,22, 2, 5,86 , 1,1, 9,16, 22, 10}; // -> 82 
	//int[] src = new int[] {86, 3, 7,22, 2, 5,4 , 1,1, 9,16, 22, 100}; // -> 82 
	
	long s_time = System.currentTimeMillis();
	int solu0 = Solution0(src);
    System.out.println(">>> (0)Final Max ABS Value is: "+ solu0);   	   	    
	long total_time = System.currentTimeMillis() - s_time;	
	System.out.println("Total Time in millissec:" + total_time );

	long s_time1 = System.currentTimeMillis();
	int solu=Solution(src);
    //System.out.println(">>> (1)Final Max ABS Value is: "+ solu);   	   	    
	long total_time1 = System.currentTimeMillis() - s_time1;	
	System.out.println("Total Time in millissec:" + total_time1 );
	
	if (solu0!=solu){
		System.out.println("***** ALERT *********** solu0 =" + solu0 + " ***** solu = " + solu);
		System.out.println(src);
	}
//	else{
//		System.out.println("***<< GOOD MATCH >> ***");
//	}
	}

	System.out.println("end");
	}
	
}

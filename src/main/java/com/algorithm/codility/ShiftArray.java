package com.algorithm.codility;

import java.util.Scanner;

public class ShiftArray {
	/*
	 * 3 2 // 3=# elements input; 2=shift pos 3 2 344 210 102 --> 102 344 210
	 */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt(); // num of input elements
		int d = in.nextInt();
		int[] a = new int[n]; // output array
		int[] o = new int[n]; // input

		for (int i = 0; i < n; i++) {
			o[i] = in.nextInt();
		}

		for (int a_i = 0; a_i < o.length; a_i++) {
			int pos = a_i + d;
			if (pos >= o.length) {
				pos = pos - o.length;
			}
			a[a_i] = o[pos];
		}

		for (int k = 0; k < a.length; k++) {
			System.out.print(a[k] + " ");
		}

	}
}

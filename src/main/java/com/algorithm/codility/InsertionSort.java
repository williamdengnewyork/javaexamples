package com.algorithm.codility;

import java.util.Random;

import org.junit.Test;

import com.algorithm.codility.Employee;

public class InsertionSort {

	public static Employee[] hireEmployees(int size) {
		Employee[] employees = new Employee[size];
		Random random = new Random();
		for (int i = 0; i < size; i++) {
			int empNum = random.nextInt(1000000000);
			
			Employee emp = new Employee(empNum, "FirstName" + empNum,
			 "LastName" + empNum, "@facebook.com" + empNum);
						
			employees[i] = emp;
			System.out.println(emp.employeeNumber);
		}
		return employees;
	}


	public static Employee[] sortEmployeesByInsertionSort(Employee[] employees) {
		System.out.println("Sorted by Insertion Sort Algorithm");
		Employee empTmp;
		for (int i = 0; i < employees.length; i++) {
			int j = i;
			while (j >= 1 && employees[j - 1].employeeNumber > employees[j].employeeNumber) {
				empTmp = employees[j - 1];
				employees[j - 1] = employees[j];
				employees[j] = empTmp;
				j = j - 1;
			}
		}
		return employees;
	}

	@Test
	public void main() {
		Employee[] employees = InsertionSort.sortEmployeesByInsertionSort(InsertionSort.hireEmployees(5));
		for (Employee employee : employees) {
			System.out.println(employee.employeeNumber);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	// inner class  -- had problem initialize it within
	/*
	public class Employee {
		int employeeNumber;
		String firstName;
		String lastName;
		String emailId;

		public Employee() {

		}

		public Employee(int employeeNumber, String firstName, String lastName, String emailId) {
			this.employeeNumber = employeeNumber;
			this.firstName = firstName;
			this.lastName = lastName;
			this.emailId = emailId;
		}
	}
	*/

}

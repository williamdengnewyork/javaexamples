package com.algorithm.codility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import javax.print.attribute.Size2DSyntax;

public class Solution {

	static int [] initArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
	
	public static void shiftleft(){
		int intoWalk = 21;
		
	    //sSystem.out.println(new ArrayList(moveArray));
	    
		for (int i = 0; i < initArray.length-1; i++) {
		    initArray[i] = initArray[i+1];
		}
		initArray[initArray.length - 1] = intoWalk;

		for (int i=0; i<initArray.length; i++)
		    System.out.println(initArray[i]);
		
	}
	

	public static void shiftright(){
		int intoWalk = 21;

		 int moveValue=-999;

		for (int i = initArray.length-1; i>0; i--) {
		    if (i== initArray.length-1){
		    	moveValue=initArray[i]; 
		    }
			initArray[i] = initArray[i-1];
		    
		}
		initArray[0] = moveValue;

		for (int i=0; i<initArray.length; i++)
		    System.out.println(initArray[i]);
		
	}

	
	 public static int[] CyclicRotation ( int K) { // expensive time complexity
		
		 int moveValue=-999;
		 
		 for (int k=1 ; k<=K; k++){
				for (int i = initArray.length-1; i>0; i--) {
				    if (i== initArray.length-1){
				    	moveValue=initArray[i]; 
				    }
					initArray[i] = initArray[i-1];
				    
				}
				initArray[0] = moveValue;
			 
		 }

		 for (int i=0; i<initArray.length; i++)
			    System.out.println(initArray[i]);

		return initArray;
		 
	 }

	 public static int[] CyclicRotationOne ( int K) {
		
		 if (initArray.length==0){
			 return initArray;
		 }
		 int moveValue=-999;
		 
				for (int i = initArray.length-1; i>K; i--) {
				    if (i== initArray.length-1){
				    	moveValue=initArray[i]; 
				    }
					initArray[i] = initArray[i-K];
				    
				}
				initArray[0] = moveValue;
			 

		 for (int i=0; i<initArray.length; i++)
			    System.out.println(initArray[i]);

		return initArray;
		 
	 }
	 
	 public static int frogJump(int X, int Y, int D){
		 if (((Y-X) % D)==0)
			 return ( (Y-X) / D );
		 else
			 return ( (Y-X) / D ) + 1 ;
	 }
	 
	 public static int equi(int arr[], int k) {

		 	int n=arr.length; 
		    if (n==0) return -1; 
		    
		    long sum = 0;
		    int i; 
		    for(i=0;i<n;i++) {
		    	sum+=(long) arr[i]; 
		    }

		    int cnt = 0;
		    long sum_left = 0;    
		    for(i=0;i<n;i++) {
		        long sum_right = sum - sum_left - (long) arr[i];
		        if (sum_left == sum_right){
		        	cnt++;
		        	if (cnt==k){
		        		return i;
		        	}
		        }
		        sum_left += (long) arr[i];
		    } 
		    return -1; 
		} 

	 public static int equi(int arr[]) {

		 	int n=arr.length; 
		    if (n==0) return -1; 
		    
		    long sum = 0;
		    int i; 
		    for(i=0;i<n;i++) {
		    	sum+=(long) arr[i]; 
		    }

		    long sum_left = 0;    
		    for(i=0;i<n;i++) {
		        long sum_right = sum - sum_left - (long) arr[i];
		        if (sum_left == sum_right){
		        		return i;
		        }
		        sum_left += (long) arr[i];
		    } 
		    return -1; 
		} 
	 
	 
	    public static <E> int cover(int[] A) {
	    	int size = A.length;
	    	Set tset_all = new TreeSet<E>();
	    	Set tset_short = new TreeSet<E>();
	    	
	    	//loop1, get unique value -> Treeset1
	    	for (int i=0 ; i<=size-1; i++){
	    		tset_all.add(A[i]);
	    	}	    	
	    	//System.out.println(tset_all);
	    	
	    	//loop2, get add to set2 and check size, or compare set1 and set2
	    	for (int k=0 ; k<=size-1; k++){
	    		tset_short.add(A[k]);
	    		
	    		//if (tset_short.containsAll(tset_all)){
	    		if (tset_short.size() == tset_all.size()){
	    	    //	System.out.println(tset_short);
	    			return k;
	    		}
	    	}	    	
	    	
			return -1;
	    }

	    //‘NumberOfDiscIntersection
	    public static <E> int discoverlappair(long[] A) { // time O(N^2), space O(1)
	    	int size = A.length;
	    	int cnt=0;
	    	long a=0;
	    	long b=0;
	    	
	    	for (int i=0 ; i<=(size-1); i++){
	    		a=A[i];
	    		for (int k=i+1; k<=(size-1); k++){
	    			b=A[k];
	    			if ((k-i) <= (a + b)){
	    				cnt++;
	    		    	if (cnt >  10000000) {
	    		    		return -1;
	    		    	}	 
	    			}
	    		}
	    		
	    	}	    		    	
			return cnt;
	    }

	    // ‘NumberOfDiscIntersection  --BAD
	    static int solution(long[] A) // time O(N^2), space O(1)
	    {             // simple-minded way: check if each (unordered) pair is intersecting
	        int n = A.length;
	        int numIntersecs = 0;
	        for (int i = 0; i < n - 1; i++)
	            for (int j = i + 1; j < n; j++)
	                if (i - A[i] <= j + A[j] && j - A[j] <= i + A[i]) //just touching is intersection
	                    if (numIntersecs == 10E6)
	                        return -1;
	                    else
	                        numIntersecs++;
	        return numIntersecs;
	    }

	    //‘NumberOfDiscIntersection
	    static int solution2(long[] A) // time O(N*Log(N)), space O(N)
	    {            // decrement max number of pairs by number of non-intersecting pairs
	        int n = A.length;
	        if (n < 2)
	            return 0;
	        long numIntersecs = n * (n - 1) / 2; // unordered pairs, initialized to max possible
	        long[] hiVals = new long[n];
	        long[] loVals = new long[n];
	        for (int i = 0; i < n; i++)
	        {
	            hiVals[i] = i + A[i]; // high value of disk edge (along x-axis)
	            loVals[i] = i - A[i]; // low value of disk edge (along x-axis)
	        }
	        Arrays.sort(hiVals);
	        Arrays.sort(loVals);
	        int jLo = 0; // initialize inner iterator only once
	        for (int iHi = 0; iHi < n; iHi++)
	        {
	            for ( ; jLo < n; jLo++) // nested, but only cycled thru once (or twice, sorta)
	            {
	                if (loVals[jLo] > hiVals[iHi]) // disks don't intersect if one's lo > other's hi
	                {
	                    numIntersecs -= n - jLo; // decrement by the num of lo values > this hi value
	                    break; // don't increment iterator, check this low value again next time
	                }
	            }
	            if (jLo == n)
	                break; // no more low values > high values
	        }
	        if (numIntersecs > 10E6)
	            return -1;
	        else
	           return (int) numIntersecs;
	    }
	    
	    //OddOccuranceInArray
	    static int odd(int[] A) // time O(N)
	    {
	    	int n = A.length;
	    	System.out.println(Arrays.toString(A));
	        Arrays.sort(A);
	    	System.out.println(Arrays.toString(A));

	        int prv_value = -1;
	        for (int i = 0; i <= n - 1; i++){   /// !!!! i<= n-1 !!!
	        	if (i%2==0){
	        		prv_value = A[i];
	        	}else{
	        		if (A[i]==prv_value){
	        			continue;	        			
	        		}else {
	        			return prv_value;
					}
	        	}
	        }	        
	        return prv_value;
	    }
	    
	    //CheckPermu
	    static int chkPermu(int[] A) // time O(N)
	    {
	    	int n = A.length;
	    	System.out.println(Arrays.toString(A));
	        Arrays.sort(A);
	    	System.out.println(Arrays.toString(A));

	        int permu = 1;
	        for (int i = 0; i <= n - 1; i++){   /// !!!! i<= n-1 !!!
	        	if (A[i]==(i+1)){
	        		continue;
	        	}else{
	        		return 0;
	        	}
	        }	        
	        return permu;
	    }
	    
	    
	    //MaxProductOfThreeNumbers
	    public static int MaxProductOfThreeNumbers(int[] A) { // O(nLn(n))
	        Arrays.sort(A);   // O(nLn(n))
	        int length = A.length;
	        int  P, Q, R;
	        int maximumLeft = Integer.MIN_VALUE; 
	        int maximumRight = Integer.MAX_VALUE;

	        P = A[length - 3];
	        Q = A[length - 2];
	        R = A[length - 1];
	        maximumRight = P * Q * R;

	        P = A[0];
	        Q = A[1];
	        R = A[length -1];

	        maximumLeft = P * Q * R;
	        return maximumRight > maximumLeft ? maximumRight : maximumLeft;
	    }
	    
	    //MaxProductOfThreeNumbers
	    public static int MaxProductOfThreeNumbers2(int[] A) {
	        int N = A.length;	        
	        Arrays.sort(A); // the worst-case time complexity is O(N*log(N))

	        // the max product of three elements is the product of the last three
	        // elements in the sorted array or the product of the first two elements
	        // and the last element if the first two elements are negatives.
	        return Math.max(A[0] * A[1] * A[N-1], A[N-3] * A[N-2] * A[N-1]);
	    }
	    
	    //MaxProductOfThreeNumbers  -- O(n)
	    public static int MaxProductOfThreeNumbers3(int[] a) {
	        int min1 = Integer.MAX_VALUE;
	        int min2 = Integer.MAX_VALUE;
	        
	        int max1 = Integer.MIN_VALUE;
	        int max2 = Integer.MIN_VALUE;
	        int max3 = Integer.MIN_VALUE;
	        
	        //if size <3, return 0
	        //if size=3
	        //if size=4
	        //if size=5
	        
	        for (int i = 0; i < a.length; i++) {
	        	 if (a[i] > max1) {
	        	    //if a[i] is greater than the biggest max then a chain reaction is started,
	        	    // max3 will be max2, max2 will be actual max1 and max1 will be a[i]    
	        	     max3=max2;
	        	     max2=max1;
	        	     max1=a[i];
	        	 }else if(a[i]>max2){
	                 max3 = max2;
	                 max2 = a[i];
	        	 }else if(a[i]>max3){
	                 max3 = a[i];
	             }
	        	 
	        	 if (a[i] < min1) {
	                 min2 =min1;
	                 min1=a[i];
	             } else if (a[i] < min2) {
	                 min2 = a[i];
	             }	         
	        }
	        
	        int prod1 = min1 * min2 * max1;
	        int prod2 = max1 * max2 * max3;
	    	
	        return prod1 > prod2 ? prod1 : prod2;	    	
	    	//return Math.max(prod1, prod2);
	    }

	    
	    // Method for getting the maximum value
	    public static int getMax(int[] inputArray){ 
	      int maxValue = inputArray[0]; 
	      for(int i=1;i < inputArray.length;i++){ 
	        if(inputArray[i] > maxValue){ 
	           maxValue = inputArray[i]; 
	        } 
	      } 
	      return maxValue; 
	    }
	   
	    // Method for getting the minimum value
	    public static int getMin(int[] inputArray){ 
	      int minValue = inputArray[0]; 
	      for(int i=1;i<inputArray.length;i++){ 
	        if(inputArray[i] < minValue){ 
	          minValue = inputArray[i]; 
	        } 
	      } 
	      return minValue; 
	    } 
	    
	    public static void main(String[] args) throws InterruptedException {				
			long s_time =System.currentTimeMillis();			
			System.out.println(" shiftright =========================");
			//shiftright();
			System.out.println(" shiftleft ==========================");
			//shiftleft();
			System.out.println(" CyclicRotation(3)  ==========================");
			//CyclicRotation(3);
			System.out.println(" CyclicRotationOne(3)  ==========================");
			//CyclicRotationOne(3);
			//sSystem.out.println(" frogJumps === " + frogJump (10,60, 7));
			int [] equiArray = {-1, 3, -4, 5, 1, -6, 2, 1};
			//System.out.println(" equi == " + equi(equiArray));

			int [] coverArray = {2, 3, 1, 0, 7, 1, 3,8};
			//System.out.println(" coverArray == " + cover(coverArray));

			long [] discArray = {1, 2147483647, 0};// range limit of int: overflow fail //{1, 5, 2, 1, 4, 0, 2, 6};
			//long [] discArray = {1, 5, 2, 1, 4, 0, 2, 6};			
			//System.out.println(" discoverlappair == " + discoverlappair(discArray));			
			//System.out.println(" solution == " + solution(discArray));
			//System.out.println(" solution2 == " + solution2(discArray));
			
			//int [] oddArray = {9, 3, 8, 3, 2, 8, 7, 9, 7, 4, 3, 4, 3, 2,11};
			int[] oddArray = {9, 3, 9, 3, 11};
			//System.out.println(" odd == " + odd(oddArray));

			int[] chkPermuArray = {0, 3, 2, 5, 4, 6};
			//System.out.println("permu == " + chkPermu(chkPermuArray));
			
			int[] MaxProductOf3Array = {-10, -9, 3, 2, 5, 4, 6}; // {0, 3, 2, 5, 4, 6};
			//System.out.println("MaxProductOf3Array== " + MaxProductOfThreeNumbers(MaxProductOf3Array));
			//System.out.println("MaxProductOf3Array2== " + MaxProductOfThreeNumbers2(MaxProductOf3Array));
			//System.out.println("MaxProductOf3Array3== " + MaxProductOfThreeNumbers3(MaxProductOf3Array));

			//for ( int k=1; k <100; k++){
				
			int src[] = new int[100];
		    Random random = new Random();
		    for (int i = 0; i < src.length; i++) {
		      src[i] = random.nextInt();
		    }

		    //int[] src = new int[] {1,3,-3};  // ->6
			//int[] src = new int[] {4, 3, 2, 5, 1,1}; // -> 4
			
			int size=src.length;
			int abs = 0;
			
			System.out.println(">>> The array" + Arrays.toString(src));	
			for (int i = 0; i<size-1; i++){
				System.out.println(i);
				
		        int a1[] = Arrays.copyOfRange(src, 0, i+1);
				int b1[] = Arrays.copyOfRange(src, i+1, size);
				
				System.out.println(Arrays.toString(a1));				
				 int max_a = getMax(a1);
				System.out.println("Max　A Value is: "+max_a);

				System.out.println(Arrays.toString(b1));				
				 int max_b = getMax(b1);
				System.out.println("Max　B Value is: "+max_b);
				 				 
				if ( Math.abs(max_a - max_b) > abs){
				    	abs=Math.abs(max_a - max_b);
				};				
			    System.out.println("ABS Value is: "+ Math.abs(max_a - max_b));   
			}
		    System.out.println(">>> Final ABS Value is: "+abs);   
			    
			//}  for
			    
			//TimeUnit.SECONDS.sleep(4);		
			long total_time = System.currentTimeMillis() - s_time;
			System.out.println("Total Time in millissec:" + total_time );
		}

		
}

package com.algorithm.codility;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Solution4 {


	public static int getCountofNegSum(int[] A){
		double sum=0;
		int size=A.length;
		int count=0;
		
		for (int k=0; k<size; k++){
			for (int i=k ; i<size; i++){
				   sum = sum + A[i];
				   if (sum<-0){
					   count++;
					}		
			}
			sum = 0;
		}
	
		return count;
	}
	
	//https://www.hackerrank.com/challenges/java-1d-array-easy/forum
	public static void main(String[] args) {

		//int[] A = {1, -2, 4, -5, 1};  //1 -2 4 -5 1
		Scanner input = new Scanner(System.in);

		System.out.println("Please enter total n : ");
		int n = input.nextInt(); // getting an integer

		Scanner input2 = new Scanner(System.in);
		System.out.println("Please enter your array : ");
		String str = input2.nextLine(); // getting a String value
		System.out.println("str = " + str);

		
		String[] list = str.split(" ");
		int[] array = new int[list.length];
		for(int i = 0; i < list.length; i++){ 
			array[i] = Integer.parseInt(list[i]);
		}
		
		//System.out.println(Arrays.toString(array));
		int cnt = getCountofNegSum(array);
		System.out.println("Count of Negative Sum =" + cnt);
		
		System.out.println("end");
	}

}

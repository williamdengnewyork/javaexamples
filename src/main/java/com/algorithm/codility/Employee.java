package com.algorithm.codility;

public class Employee {
		
		int employeeNumber;
		String firstName;
		String lastName;
		String emailId;

		public Employee() {

		}

		public Employee(int employeeNumber, String firstName, String lastName, String emailId) {
			this.employeeNumber = employeeNumber;
			this.firstName = firstName;
			this.lastName = lastName;
			this.emailId = emailId;
		}

		public int getEmployeeNumber() {
			return employeeNumber;
		}

		public void setEmployeeNumber(int employeeNumber) {
			this.employeeNumber = employeeNumber;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getEmailId() {
			return emailId;
		}

		public void setEmailId(String emailId) {
			this.emailId = emailId;
		}
	
	
	
}

package com.algorithm.codility;

public class FrogJumpSteps {

    public int solution(int X, int Y, int D) {
	      double r= (Y - X)% D ;
		if (r>0){
	    	return  ((Y - X) / D) +1;
			
		}else{
	    	return  (Y - X) / D ;
			
		}
			
    }
	
	public static void main(String[] args) {
		FrogJumpSteps f = new FrogJumpSteps();
		System.out.println(f.solution(10,70,30));
	}

}

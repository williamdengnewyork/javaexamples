package com.algorithm.codility;


//Input:  1, 2, 3, 4, 5
//|  |  |  |  |
//v  v  v  v  v
//Output: 0, 1, 1, 2, 3

// http://codility.kamol.org/
	
public class FibonacciNumber {

	static int fib(int n) {
	    int a = 0, b = 1, c = 0;
	    if ((n <= 0) || (n >= Integer.MAX_VALUE)) {
	      return -1;
	    } else if (n == 1) {
	      return 0;
	    } else if (n == 2) {
	      return 1;
	    } else {
	      for (int i = 2; i < n; i++) {
	        c = a + b;
	        a = b;
	        b = c;
	      }
	    }
	    return c;
	  }
	
	public static void main(String[] args) {

		for ( int i = 0; i < 20; i++ ) {
		      System.out.print ( fib(i) + ", " );
		    }
		    System.out.println ( fib(10) );
	}

	//-1, 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 34
    // ?-1, ? 34
}

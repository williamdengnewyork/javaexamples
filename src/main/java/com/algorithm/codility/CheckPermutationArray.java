package com.algorithm.codility;

public class CheckPermutationArray {

    public int solution(int[] A) {
        // write your code in Java SE 8
        int result=0;
        for (int i=0; i<A.length; i++){
           result += A[i]; 
        }
        if (result==(1+A.length)*A.length / 2){
            return 1;
        }else
        {
            return 0;
        }
    }
    
	public static void main(String[] args) {
		CheckPermutationArray c= new CheckPermutationArray();
		int[] A= {1,2,3,4,5};
		System.out.println(c.solution(A));

	}

}




package com.algorithm.codility;

import java.util.Arrays;

public class MergeSort {
    
    // static method
    public static int[] mergeSort(int [] list) {
        if (list.length <= 1) {
            return list;
        }
        
        // 1. Split the array in two sub-arrays
        int[] first = new int[list.length / 2];
        int[] second = new int[list.length - first.length];
        System.arraycopy(list, 0, first, 0, first.length);
        System.arraycopy(list, first.length, second, 0, second.length);
        
        // 2. Sort each half sub-array -- !!!! Recursive function call 
        mergeSort(first);
        mergeSort(second);
        
        // 3. Merge the halves together, overwriting the original array "list"
        merge(first, second, list);
        
        return list;
    }
    
    //static proc method
    private static void merge(int[] first, int[] second, int [] result) {
        // Merge both halves into the result array
        // Next element to consider in the first array
        int iFirst = 0;
        // Next element to consider in the second array
        int iSecond = 0;
        
        // Next open position in the result
        int j = 0;
        // As long as neither iFirst nor iSecond is past the end, move the
        // smaller element into the result.
        while (iFirst < first.length && iSecond < second.length) {
            if (first[iFirst] < second[iSecond]) {
                result[j] = first[iFirst];
                iFirst++;
                } else {
                result[j] = second[iSecond];
                iSecond++;
            }
            j++;
        }
        // copy what's left
        System.arraycopy(first, iFirst, result, j, first.length - iFirst);
        System.arraycopy(second, iSecond, result, j, second.length - iSecond);
        
        // return = result
    }
    
    
    
    
  //http://javahungry.blogspot.com/2013/06/java-sorting-program-code-merge-sort.html  
  //- See more at: http://www.java2novice.com/java-sorting-algorithms/merge-sort/#sthash.60HfvbqC.dpuf
    public static void main(String args[]) throws Exception
    {
        
    	int[] inputArr = {45,23,11,89,77,98,4,28,65,43};
       System.out.println("Before =" + Arrays.toString(inputArr));
    	// MergeSort s= new MergeSort();        
    	MergeSort.mergeSort(inputArr);
        System.out.println("After =" + Arrays.toString(inputArr));

	
   }
}
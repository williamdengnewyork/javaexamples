package com.algorithm.codility;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class LargestValue {

	// Arrays.sort(a[]) <- O(nlogN) - O(n^2) -- costly!

	static // 31378000-39787000
	int largestIntInArrayFaster(int a[]) { // O(nlogn)
		int len = a.length;
		int max = 0;
		if ((len & 1) != 0) {
			max = a[len - 1];
		}
		for (int i = 0, j = len - 1; i < len / 2; i++, j--) {
			if (max < a[i]) {
				max = a[i];
			}
			if (max < a[j]) {
				max = a[j];
			}
		}
		return max;
	}

	// 34542000-51291000
	static int largestIntInArray(int[] a) { // O(n)
		int max = a[0];
		for (int i = 1; i < a.length; i++) {
			if (max < a[i]) {
				max = a[i];
			}
		}
		return max;
	}

	int b[] = new int[100000000];

	@Before
	public void setUp() throws Exception {
		// LargestValue = new LargestValue();
		Random random = new Random();
		for (int i = 0; i < b.length; i++) {
			b[i] = random.nextInt();
		}
	}

	// 962296 vs 962304
	@Test
	public void memoryConsumption() {
		System.out.println("================== MEMORY ========================");
		// System.out.println("Foud lagest Value = " +
		// LargestValue.largestIntInArrayFaster(b));
		System.out.println("Foud lagest Value = " + LargestValue.largestIntInArray(b));
		// Get the Java runtime
		Runtime runtime = Runtime.getRuntime();
		// Run the garbage collector
		runtime.gc();
		// Calculate the used memory
		long memory = runtime.totalMemory() - runtime.freeMemory();
		System.out.println("Used memory is bytes: " + memory);
		System.out.println("==================================================");
	}

	// 31378000-39787000 vs 34542000-51291000
	@Test
	public void timeConsumption() {
		System.out.println("================== TIME ==========================");
		long startTime = System.nanoTime();
		// System.out.println("Foud lagest Value = " +
		// LargestValue.largestIntInArrayFaster(b));
		System.out.println("Foud lagest Value = " + LargestValue.largestIntInArray(b));
		long stopTime = System.nanoTime();
		long elapsedTime = stopTime - startTime;
		System.out.println("elapsedTime in NanoSeconds =" + elapsedTime);
		System.out.println("==================================================");
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

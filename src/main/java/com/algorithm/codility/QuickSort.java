package com.algorithm.codility;

import java.util.Arrays;

/*  
The basic step of Quick Sorting an array are as follows -  Divide and Conquer approach:

1. Select a pivot/partition position, normally the middle one -- Divide and Conquer 
2. From both ends, swap elements and make all elements on the left less than the pivot and all elements on the right greater than the pivot
3. Recursively sort left part and right part


https://en.wikipedia.org/wiki/Quicksort
*
*
*/


public class QuickSort {
	
	public static void main(String[] args) {
		int[] x = { 9, 2, 4, 7, 3, 7, 10 };
		System.out.println("Before Quick Sort: " + Arrays.toString(x));
 
		int low = 0;
		int high = x.length - 1;
 
		quickSort(x, low, high);
		System.out.println("After Quick Sort: " +  Arrays.toString(x));
	}
 
	public static void quickSort(int[] arr, int low, int high) { //O(NlongN)

		if (arr == null || arr.length == 0)
			return;
 		
		if (low >= high)
			return;
 
		// pick the pivot
		int middle = low + (high - low) / 2;
		int pivot = arr[middle];
 
		// make left < pivot and right > pivot
		int i = low, j = high;
		while (i <= j) {
			while (arr[i] < pivot) {
				i++;
			}
 
			while (arr[j] > pivot) {
				j--;
			}
 
			if (i <= j) {
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
				i++;
				j--;
			}
		}
 
		// recursively sort two sub parts
		if (low < j)
			quickSort(arr, low, j);
 
		if (high > i)
			quickSort(arr, i, high);
	}
}
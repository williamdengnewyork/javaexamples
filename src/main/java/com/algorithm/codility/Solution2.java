package com.algorithm.codility;

import java.util.Arrays;
import java.util.Date;
import java.util.Random;

import javax.swing.text.Position;
import javax.swing.text.html.HTMLDocument.HTMLReader.HiddenAction;

import org.junit.experimental.max.MaxCore;
import org.junit.experimental.theories.Theories;

public class Solution2 {

	
	
    public static int getMaxElement(int[] inputArray){  // O(n) 
	      int maxValue = inputArray[0]; 
	      for(int i=1;i < inputArray.length;i++){    
	        if(inputArray[i] > maxValue){ 
	           maxValue = inputArray[i]; 
	        } 
	      } 
	      return maxValue; 
	    }

    
    public static int Solution(int[] A){
    	int size=A.length;
    	int a1[]={};
    	int b1[]={}; 
    	int max_a=Integer.MIN_VALUE;
    	int max_b=Integer.MIN_VALUE;
    	int max_diff=0;
    	int abs = 0;
    	boolean debug=false;
    	if (debug) System.out.println(">>> The array" + Arrays.toString(A));
    	    	
    	for (int i = 0; i<size-1; i++){
    		    		
            a1 = Arrays.copyOfRange(A, 0, i+1);
    		b1 = Arrays.copyOfRange(A, i+1, size);
    		
    		max_a = getMaxElement(a1);
    		
       		max_b = getMaxElement(b1);
    		
    		max_diff = Math.abs(max_a - max_b);
    		if (max_diff  > abs){
    		    abs=max_diff;
    		}
    	}
    	return abs;
    }

    public static int Solution3(int[] a){
    	int size=a.length;
    	
    	boolean debug= false ;
    	
    	int max_hi=Integer.MIN_VALUE;
    	int sec_hi=Integer.MIN_VALUE;
    	int right_sec_hi=Integer.MIN_VALUE;
    	int max_abs_diff=0;
    	int max_pos=0;
    	
    	// 1. Finx max_hi and pos index
    	for (int n = 0; n<size; n++){
    		if (a[n]>max_hi){
    			max_hi = a[n];
    			max_pos = n;
    		}
    	}    	
    	if (debug) System.out.println("->The Max Hi value = " + max_hi + " At " + max_pos);
    	
    	// 2. Find Left sec_hi 
    	for (int i = 0; i<max_pos; i++){
    		if (debug) System.out.println(i);
    		if (a[i]>max_hi){
    			sec_hi=max_hi;
    			max_hi = a[i];    			
    		}else if((a[i]>sec_hi)&&(a[i]<max_hi)){
    			sec_hi=a[i];
    		}
    		
    		if (debug) System.out.println("Local Left ABS Value is: "+ (max_hi - sec_hi) + ((max_hi-sec_hi) > max_abs_diff));   
			if ((max_hi-sec_hi) > max_abs_diff){
				max_abs_diff = max_hi-sec_hi ;
			}
			
    	}

    	//3. Find Right sec_hi
    	if (debug) System.out.println("-->Switch to from Right side from " + (size-1) + " to "+ max_pos);
    	for (int j = size-1; j > max_pos; j--) {
    		if (debug) System.out.println(j);
    		if (a[j]>max_hi){
    			right_sec_hi=max_hi;
    			max_hi = a[j];    			
    		}else if((a[j]>right_sec_hi)&&(a[j]<max_hi)){
    			right_sec_hi=a[j];
    		}else if(a[j]==max_hi){
    			right_sec_hi=a[j];
    		}
    		
    		if (debug) System.out.println("Local Right ABS Value is: "+ (max_hi - right_sec_hi) + ((max_hi - right_sec_hi) > max_abs_diff) );   
			if ((max_hi - right_sec_hi) > max_abs_diff){
				max_abs_diff = max_hi - right_sec_hi ;
			}
		}
    	return max_abs_diff;
    }

	public static void main(String[] args) {

		System.out.println("Start......" + (new Date()) );
		boolean debug = false ;
		for (int k=1; k<20000; k++){ //Correctness: 1000, 10000 Time: 20000 for 1.5 hr both
			if (debug) System.out.println(k);	
			
			int src[] = new int[1000000]; 
		    Random random = new Random();
		    int min=-1000000000;
		    int max=1000000000;
		    for (int i = 0; i < src.length; i++) {
		      src[i] = random.nextInt((max - min) + 1) + min; 
		    }
	//int[] src =	{3, 2, 4, -2, 6, 8, -3};		    
	//int[] src = {10, 3, 7, 9, -1, 9, 3};
    //int[] src = new int[] {1,3,-3};  // ->6
	//int[] src = new int[] {4, 3, 2, 5, 1,1}; // -> 4
	//int[] src = new int[] {4, 3, 7, 2, 5,6 , 1,1, 9,13}; // -> 9
	//int[] src = new int[] {4, 3, 7,22, 2, 5,86 , 1,1, 9,16, 22, 10}; // -> 82 
	//System.out.println(">>> The array" + 7Arrays.toString(src));
		    
	long s_time = System.currentTimeMillis();
	int bruteforce = Solution(src);
	long e_time1 = System.currentTimeMillis();
	if (debug) System.out.println(">>> (1)-Solution1: Final Max ABS Value is: "+ bruteforce + " time = " + (e_time1-s_time) );   	   	    

	int oneloop=Solution3(src);
	long e_time2 = System.currentTimeMillis();
    if (debug) System.out.println(">>> (3)-Solution3: Final Max ABS Value is: "+ oneloop + " time = " + (e_time2-e_time1));   	   	    
	long total_time = System.currentTimeMillis() - s_time;	
	if (debug) System.out.println("Total Time in millissec:" + total_time );
	
	if (oneloop!=bruteforce){
		System.out.println("*********** Mismatch ******************");
		System.out.println(">>> The array" + Arrays.toString(src));
	    System.out.println(">>> (1)-bruteforceFinal Max ABS Value is: "+ bruteforce);   	   	    
	    System.out.println(">>> (3)-oneloop Final Max ABS Value is: "+ oneloop);   	   	    
		System.out.println("*********** Mismatch ******************");
	}else{
		if (debug) System.out.println(">>>>>GOOD Match!");
	}
	}
	}
}

package com.algorithm.codility;

import java.io.FileNotFoundException;
import java.io.IOException;

public class StringReversal {

    public static void main(String args[]) throws FileNotFoundException, IOException {

        //original string
        String str = "Sony is going to introduce Internet TV soon";
        System.out.println("Original String: " + str);

        //reversed string using Stringbuffer
        String reverseStr = new StringBuffer(str).reverse().toString();
        System.out.println("Reverse String in Java using StringBuffer: " + reverseStr);

        //iterative method to reverse String in Java
        reverseStr = reverse(str);
        System.out.println("Reverse String in Java using Iteration: " + reverseStr);

        //recursive method to reverse String in Java
        reverseStr = reverseRecursively(str);
        System.out.println("Reverse String in Java using Recursion: " + reverseStr);

    	char[] strChars = str.toCharArray();
    	revereseCharArray(strChars, 0, strChars.length-1);
        System.out.println("Reverse String in Java using Divide and Swap: " + String.valueOf(strChars));

        
    }

    // 1 - O(n)  -- use StringBuilder and reverse for loop on String char
    public static String reverse(String str) {    
        StringBuilder strBuilder = new StringBuilder();
        char[] strChars = str.toCharArray();

        for (int i = strChars.length - 1; i >= 0; i--) {
            strBuilder.append(strChars[i]);
        }

        return strBuilder.toString();
    }

    // 2 - Recursive function !!! // O(n) ?
    ////Read more: http://javarevisited.blogspot.com/2012/01/how-to-reverse-string-in-java-using.html#ixzz4Cigr2wcm
    public static String reverseRecursively(String str) { 

        //base case to handle one char string and empty string
        if (str.length() < 2) {
            return str;
        }

        //String inter = str.substring(1) + str.charAt(0);
        //System.out.println(inter);
        
        return reverseRecursively(str.substring(1)) + str.charAt(0);
    }
    
    //3 - Divide in middle and Symetric swap
    //http://www.geeksforgeeks.org/write-a-program-to-reverse-an-array-or-string/
    /* Function to reverse arr[] from start to end*/
    public static void revereseCharArray(char[] strChars, int start, int end)
    {
        char temp;
        if (start >= end)
            return;
        temp = strChars[start];
        strChars[start] = strChars[end];
        strChars[end] = temp;
        revereseCharArray(strChars, start+1, end-1);
    }

    public static void rvereseArray(int arr[], int start, int end)
    {
        int temp;
        if (start >= end)
            return;
        temp = arr[start];
        arr[start] = arr[end];
        arr[end] = temp;
        rvereseArray(arr, start+1, end-1);
    }
}



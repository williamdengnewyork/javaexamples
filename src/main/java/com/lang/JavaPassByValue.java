package com.lang;

//http://www.javaworld.com/article/2077424/learn-java/does-java-pass-by-reference-or-pass-by-value.html
public class JavaPassByValue {

	class Point{
		int x,y;
		
		Point(int x, int y){
			this.x=x;
			this.y=y;
		}
	}
	
	
	public void badSwap(int var1, int var2)
	{
	  int temp = var1;
	  var1 = var2;
	  var2 = temp;
	}
	
	// Pass Obj as reference copy
	public void tricky(Point arg1, Point arg2) {
		arg1.x = 100;
		arg1.y = 100;
		Point temp = arg1;
		arg1 = arg2;
		arg2 = temp;   // this DOES NOT Change, because swap reference 
	}

	public static void main(String[] args) {
		JavaPassByValue jpbv = new JavaPassByValue();
		// Primitive type -- Pass by Value 
		int a=2, b=3;
		jpbv.badSwap(a,b);
		System.out.println("badswap()" + a + " - " + b);  // does swap
		
		// Object type -- Pass by Value 
		Point pnt1 = jpbv.new Point(0, 0);    // inner class init
		Point pnt2 = jpbv.new Point(0, 0);
		
		System.out.println("Pnt1 - "+ "X: " + pnt1.x + " Y: " + pnt1.y);
		System.out.println("Pnt2 - "+ "X: " + pnt2.x + " Y: " + pnt2.y);
		System.out.println(" ");
		jpbv.tricky(pnt1, pnt2);
		System.out.println("Pnt1 - "+ "X: " + pnt1.x + " Y:" + pnt1.y);
		System.out.println("Pnt2 - "+ "X: " + pnt2.x + " Y: " + pnt2.y);
	}

}

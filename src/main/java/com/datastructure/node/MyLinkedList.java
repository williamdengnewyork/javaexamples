package com.datastructure.node;

//**** Custom Implementation of LinkedList, with common methods like add(), remove(), getcount() 
public class MyLinkedList {
 
	//**** Inner Class -- The Node is element of LinkedList
	private class Node {
		// reference to the next node in the chain, or null if there isn't one.
		Node next;
 
		// data carried by this node. could be of any type you need.
		Object data;
 
		// Node constructor
		public Node(Object dataValue) {
			next = null;
			data = dataValue;
		}
 
		// another Node constructor if we want to specify the node to point to.
		@SuppressWarnings("unused")
		public Node(Object dataValue, Node nextValue) {
			next = nextValue;
			data = dataValue;
		}
 
		// these methods should be self-explanatory
		public Object getData() {
			return data;
		}
 
		@SuppressWarnings("unused")
		public void setData(Object dataValue) {
			data = dataValue;
		}
 
		public Node getNext() {
			return next;
		}
 
		public void setNext(Node nextValue) {
			next = nextValue;
		}
	}
	//**** Inner Class -- The Node is element of LinkedList

	
	
	private static int counter;
	private Node head;
 
	// Default constructor
	public MyLinkedList() {
 
	}

	public MyLinkedList(Node firstNode) {
		 head = firstNode;
	}

	// appends the specified element to the end of this list.
	public void add(Object data) {
 
		// Initialize Node only incase of 1st element
		if (head == null) {
			head = new Node(data);
		}
 
		Node crunchifyTemp = new Node(data);
		Node crunchifyCurrent = head;
 
		// Let's check for NPE before iterate over crunchifyCurrent
		if (crunchifyCurrent != null) {
 
			// starting at the head node, crawl to the end of the list and then add element after last node
			while (crunchifyCurrent.getNext() != null) {
				crunchifyCurrent = crunchifyCurrent.getNext();
			}
 
			// the last node's "next" reference set to our new node
			crunchifyCurrent.setNext(crunchifyTemp);
		}
 
		// increment the number of elements variable
		incrementCounter();
	}
 
	private static int getCounter() {
		return counter;
	}
 
	private static void incrementCounter() {
		counter++;
	}
 
	private void decrementCounter() {
		counter--;
	}
 
	// inserts the specified element at the specified position in this list
	public void add(Object data, int index) {
		Node crunchifyTemp = new Node(data);
		Node crunchifyCurrent = head;
 
		// Let's check for NPE before iterate over crunchifyCurrent
		if (crunchifyCurrent != null) {
			// crawl to the requested index or the last element in the list, whichever comes first
			for (int i = 0; i < index && crunchifyCurrent.getNext() != null; i++) {
				crunchifyCurrent = crunchifyCurrent.getNext();
			}
		}
 
		// set the new node's next-node reference to this node's next-node reference
		crunchifyTemp.setNext(crunchifyCurrent.getNext());
 
		// now set this node's next-node reference to the new node
		crunchifyCurrent.setNext(crunchifyTemp);
 
		// increment the number of elements variable
		incrementCounter();
	}
 
	public Node getFirstNode(){
		return head;
	}

	public Object get(int index)
	// returns the element at the specified position in this list.
	{
		// index must be 1 or higher
		if (index < 0)
			return null;
		Node crunchifyCurrent = null;
		if (head != null) {
			crunchifyCurrent = head.getNext();
			for (int i = 0; i < index; i++) {
				if (crunchifyCurrent.getNext() == null)
					return null;
 
				crunchifyCurrent = crunchifyCurrent.getNext();
			}
			return crunchifyCurrent.getData();
		}
		return crunchifyCurrent;
 
	}
 
	// removes the element at the specified position in this list.
	public boolean remove(int index) {
 
		// if the index is out of range, exit
		if (index < 1 || index > size())
			return false;
 
		Node crunchifyCurrent = head;
		if (head != null) {
			for (int i = 0; i < index; i++) {
				if (crunchifyCurrent.getNext() == null)
					return false;
 
				crunchifyCurrent = crunchifyCurrent.getNext();
			}
			crunchifyCurrent.setNext(crunchifyCurrent.getNext().getNext());
 
			// decrement the number of elements variable
			decrementCounter();
			return true;
 
		}
		return false;
	}
 
	// returns the number of elements in this list.
	public int size() {
		return getCounter();
	}
 
	public String toString() {
		String output = "";
 
		if (head != null) {
			Node crunchifyCurrent = head.getNext();
			while (crunchifyCurrent != null) {
				output += "[" + crunchifyCurrent.getData().toString() + "]";
				crunchifyCurrent = crunchifyCurrent.getNext();
			}
 
		}
		return output;
	}
	
	// Algo Problem: http://stackoverflow.com/questions/10707352/interview-merging-two-sorted-singly-linked-list
	//
	// Give two "Sorted" linkedList's head nodes, Merge them (samll to big) to create new LinkedList Node
	public Node MergeListsRecursive(Node list1, Node list2) {
		  if (list1 == null) return list2;
		  if (list2 == null) return list1;

		  if (Integer.valueOf((String) list1.data) < Integer.valueOf((String) list2.data)) {
		    list1.next = MergeListsRecursive(list1.next, list2);
		    return list1;
		  } else {
		    list2.next = MergeListsRecursive(list2.next, list1);
		    return list2;
		  }
	}
	
	
	// Give two "Sorted" linkedList's head nodes, Merge them (samll to big) to create new LinkedList Node -- Non Recursive 
	// Doesn't work!
	public Node MergeListsIterative(Node list1, Node list2) {
		  if (list1 == null) return list2;
		  if (list2 == null) return list1;
    System.out.println("Iteractive...");
		  Node head;
		  if ((Integer.valueOf((String) list1.data) < Integer.valueOf((String) list2.data))) {
		    head = list1;
		  } else {
		    head = list2;
		    list2 = list1;
		    list1 = head;
		  }
		  while(list1.next != null) {
			if (Integer.valueOf((String) list1.next.data) > Integer.valueOf((String) list2.data)){
		    //if (list1.next.data > list2.data) {
		      Node tmp = list1.next;
		      list1.next = list2;
		      list2 = tmp;
		    }
		    list1 = list1.next;
		  } 
		  if (list1.next == null) list1.next = list2;
		  return head;
		}
	
	

	
	
	public static MyLinkedList crunchifyList;
	public static MyLinkedList crunchifyList2;
	 	
	public static void main(String[] args) {
 
		// Default constructor - let's put "0" into head element.
		crunchifyList = new MyLinkedList(); 
		// add more elements to LinkedList
		crunchifyList.add("3");
		crunchifyList.add("4");
		crunchifyList.add("7");
		crunchifyList.add("11");
		crunchifyList.add("13");
		System.out.println("Print: myLinkedList: \t\t" + crunchifyList);
	

		crunchifyList2 = new MyLinkedList(); 
		// add more elements to LinkedList
		crunchifyList2.add("1");
		crunchifyList2.add("2");
		crunchifyList2.add("5");
		crunchifyList2.add("8");
		crunchifyList2.add("10");
		System.out.println("Print: myLinkedList2: \t\t" + crunchifyList2);

		//Node list1 = crunchifyList.get(0);
		//Node list2 = crunchifyList2.get(0);
		//System.out.println("Print: list1: \t\t" + crunchifyList.getFirstNode());
		//System.out.println("Print: list2: \t\t" + list2);

		Node mergedNode = crunchifyList.MergeListsRecursive(crunchifyList.getFirstNode(), crunchifyList2.getFirstNode());
		System.out.println("Print: mergedNode1: \t\t" +  new MyLinkedList(mergedNode));

		mergedNode = crunchifyList.MergeListsIterative(crunchifyList.getFirstNode(), crunchifyList2.getFirstNode());
		System.out.println("Print: mergedNode2: \t\t" +  new MyLinkedList(mergedNode));
		
		
		
		
		/*
		 * Please note that primitive values can not be added into LinkedList directly. They must be converted to their
		 * corresponding wrapper class.
		 */
 
		/*
		System.out.println("Print: myLinkedList: \t\t" + crunchifyList);
		System.out.println(".size(): \t\t\t\t" + crunchifyList.size());
		System.out.println(".get(3): \t\t\t\t" + crunchifyList.get(3) + " (get element at index:3 - list starts from 0)");
		System.out.println(".remove(2): \t\t\t\t" + crunchifyList.remove(2) + " (element removed)");
		System.out.println(".get(3): \t\t\t\t" + crunchifyList.get(3) + " (get element at index:3 - list starts from 0)");
		System.out.println(".size(): \t\t\t\t" + crunchifyList.size());
		System.out.println("Print again: crunchifyList: \t" + crunchifyList);
		*/
		
	}
	
}
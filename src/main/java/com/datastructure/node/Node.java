package com.datastructure.node;

// Node implementation of LinkedList feature, and methods add(), remove(), etc
public class Node {
	Node next = null;
	int data;

	public Node(int d) {
		data = d;
	}

	// create new node (value d), add to the end
	public void addToEnd(int d) {
		Node end = new Node(d);
		Node n = this;
		while (n.next != null) {
			n = n.next;
		}
		n.next = end;
	}

	// given the first node in linkedlist, delete the node with value d, return new Node
	public Node deleteNode(Node head, int d) {
		 Node n = head;
		 if (n.data == d) {
			 return head.next; /* moved head */
		 }
		 
		 while (n.next != null) {
			 if (n.next.data == d) {
				 n.next = n.next.next;
				 return head; /* head didn�t change */
			 }
			 n = n.next;
		 }
		 return n;
	}

	
	public static void main(String[] args) {
		// create linkedlist with values
		System.out.println("create linkedlist with values 1 - 5");
		//*** A linkedList object is the first Node in the list
		Node linkedList = new Node(1);
		linkedList.addToEnd(2);
		linkedList.addToEnd(3);
		linkedList.addToEnd(4);
		linkedList.addToEnd(5);
		
		Node first=linkedList;

		if (linkedList!=null){
			System.out.println(linkedList.data);
		}
		while (linkedList.next!=null){
			System.out.println(linkedList.next.data);
			linkedList=linkedList.next;
		}
		
		System.out.println("delete a node of value 4");
		// delete a node
		Node newNode = linkedList.deleteNode(first, 4);
		
		if (newNode!=null){
			System.out.println(newNode.data);
		}
		while (newNode.next!=null){
			System.out.println(newNode.next.data);
			newNode=newNode.next;
		}

	}

}

package com.datastructure.tree;

import java.util.ArrayList;
import java.util.List;

// Java 8 doesn't have Tree data structure!

// Create your own Tree structure implementing Iterable 
//http://stackoverflow.com/questions/3522454/java-tree-data-structure
	

// A Tree - a special Node that has No parent!
public class Tree<T> {
    private Node<T> root;

    public Tree(T rootData) {
        root = new Node<T>();
        root.data = rootData;
        root.children = new ArrayList<Node<T>>();
    }

    // Characteristic of Node in Tree:
    //1. Has own data
    //2. Has One parent Node
    //3. Has One or more immediate children
    public static class Node<T> {
        private T data;
        private Node<T> parent;
        private List<Node<T>> children;
    }
}
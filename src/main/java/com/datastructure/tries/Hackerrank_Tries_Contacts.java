
package com.datastructure.tries;

import java.util.Arrays;
import java.util.Scanner;

public class Hackerrank_Tries_Contacts {

	// ***Entity Design - Trie Tree with array
	class TrieNode {
		TrieNode[] arr;
		char _c;
		boolean isEnd;
		int numChildren = 0;

		// Contructor - Initialize your data structure here.
		public TrieNode() {
			this.arr = new TrieNode[26];
		}

		@Override
		public String toString() {
			return Arrays.toString(this.arr);
		}
	}

	private static TrieNode root;
	static int cnt = 0;
	private static Scanner in;

	public Hackerrank_Tries_Contacts() {
		root = new TrieNode();
	}

	// Inserts a word into the trie.
	public void insert(String word) {
		TrieNode p = root;
		for (int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);
			int index = c - 'a';
			if (p.arr[index] == null) {
				TrieNode temp = new TrieNode();
				temp._c = c;
				p.arr[index] = temp;
				p.numChildren++;
				p = temp;
			} else {
				p.numChildren++;
				p = p.arr[index];
			}


		}
		p.isEnd = true;
		// System.out.println("insert - " + word);
		return;
	}

	// Returns if the word is in the trie.
	public static boolean search(String word) {
		TrieNode p = searchNode(word);
		if (p == null) {
			return false;
		} else {
			countSubTrieNote(p);
			return true;
		}

	}

	public static TrieNode searchNode(String s) {
		TrieNode p = root;
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			int index = c - 'a';
			if (p.arr[index] != null) {
				p = p.arr[index];
			} else {
				return null;
			}
		}

		if (p == root)
			return null;
		return p;
	}

	public static void countSubTrieNote(TrieNode p) {

		cnt = p.numChildren;
		/*
		 * if (p.isEnd) { cnt++; } TrieNode[] nodes = p.arr; for (int i = 0; i <
		 * nodes.length; i++) { TrieNode node = nodes[i]; if (node != null) {
		 * countSubTrieNote(node); } }
		 */

		return;
	}

	// https://www.hackerrank.com/challenges/ctci-contacts
	// Submit Reuslt: total 13 test -- 9 pass, 4 timeout
	public static void main(String[] args) {

		/*
		 * 4 add hack add hackerrank find hac find hak
		 */
		Hackerrank_Tries_Contacts arraytrieDictionary = new Hackerrank_Tries_Contacts();
		Hackerrank_Tries_Contacts.cnt = 0;

		try {
			in = new Scanner(System.in);
			int n = in.nextInt(); // num of input elements

			for (int i = 0; i < n + 1; i++) {
				String instruction = in.nextLine();
				if (instruction.length() >= 4) {
					if (instruction.substring(0, 3).equals("add")) {
						arraytrieDictionary.insert(instruction.substring(4));
					}
					if (instruction.substring(0, 4).equals("find")) {
						Hackerrank_Tries_Contacts.search(instruction.substring(5));
						System.out.println(Hackerrank_Tries_Contacts.cnt);
						Hackerrank_Tries_Contacts.cnt = 0;
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

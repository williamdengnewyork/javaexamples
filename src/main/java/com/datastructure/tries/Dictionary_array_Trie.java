package com.datastructure.tries;

import java.util.Arrays;

public class Dictionary_array_Trie {

	// *** key Entity Design - Trie Tree with array
	class TrieNode {
		TrieNode[] arr;
		char _c;
		boolean isEnd;

		// Contructor - Initialize your data structure here.
		public TrieNode() {
			this.arr = new TrieNode[26];
		}

		@Override
		public String toString() {
			return Arrays.toString(this.arr);
		}
	}

	private TrieNode root;

	public boolean allowPartialSearch = true;

	public Dictionary_array_Trie() {
		root = new TrieNode();
	}

	// Inserts a word into the trie.
	public void insert(String word) {
		TrieNode p = root;
		for (int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);
			int index = c - 'a';
			if (p.arr[index] == null) {
				TrieNode temp = new TrieNode();
				temp._c = c;
				p.arr[index] = temp;
				p = temp;
			} else {
				p = p.arr[index];
			}
		}
		p.isEnd = true;

		return;
	}

	// Returns if the word is in the trie.
	public boolean search(String word) {
		TrieNode p = searchNode(word);
		if (p == null) {
			return false;
		} else {
			if (allowPartialSearch) {
				return true;
			} else {
				if (p.isEnd)
					return true;
				else
					return false;
			}
		}

	}

	// Returns if there is any word in the trie
	// that starts with the given prefix.
	public boolean startsWith(String prefix) {
		TrieNode p = searchNode(prefix);
		if (p == null) {
			return false;
		} else {
			return true;
		}
	}

	public TrieNode searchNode(String s) {
		TrieNode p = root;
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			int index = c - 'a';
			if (p.arr[index] != null) {
				p = p.arr[index];
			} else {
				return null;
			}
		}

		if (p == root)
			return null;

		// System.out.println(p.toString());

		return p;
	}

	public static void main(String[] args) {

		Dictionary_array_Trie arraytrieDictionary = new Dictionary_array_Trie();
		arraytrieDictionary.allowPartialSearch = true;

		// System.out.println('c' - 'a');
		arraytrieDictionary.insert("love");
		arraytrieDictionary.insert("hackerrank");
		arraytrieDictionary.insert("holdon");

		// arraytrieDictionary.insert("your");

		if (arraytrieDictionary.search("love")) {
			System.out.println("yes - love");
		} else {
			System.out.println("no - love");
		}

		if (arraytrieDictionary.search("you")) {
			System.out.println("yes - you");
		} else {
			System.out.println("no - you");
		}

		if (arraytrieDictionary.search("hacker")) {
			System.out.println("yes - hac");
		} else {
			System.out.println("no - hac");
		}

		// System.out.println("searchNode : " +
		// arraytrieDictionary.searchNode("love"));

	}

}

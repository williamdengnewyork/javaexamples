package com.datastructure.tries;

import java.util.Arrays;
import java.util.Scanner;

public class Hackerrank_Tries_Contacts_slow {

	// ***Entity Design - Trie Tree with array
	class TrieNode {
		TrieNode[] arr;
		char _c;
		boolean isEnd;

		// Contructor - Initialize your data structure here.
		public TrieNode() {
			this.arr = new TrieNode[26];
		}

		@Override
		public String toString() {
			return Arrays.toString(this.arr);
		}
	}

	private TrieNode root;
	public boolean allowPartialSearch = true;
	int cnt = 0;
	private static Scanner in;

	public Hackerrank_Tries_Contacts_slow() {
		root = new TrieNode();
	}

	// Inserts a word into the trie.
	public void insert(String word) {
		TrieNode p = root;
		for (int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);
			int index = c - 'a';
			if (p.arr[index] == null) {
				TrieNode temp = new TrieNode();
				temp._c = c;
				p.arr[index] = temp;
				p = temp;
			} else {
				p = p.arr[index];
			}
		}
		p.isEnd = true;

		// System.out.println("insert - " + word);
		return;
	}

	// Returns if the word is in the trie.
	public boolean search(String word) {
		TrieNode p = searchNode(word);
		if (p == null) {
			return false;
		} else {
			if (allowPartialSearch) {
				countSubTrieNote(p);
				return true;
			} else {
				if (p.isEnd) {
					// countSubTrieNote(p);
					return true;
				} else {
					return false;
				}
			}
		}

	}


	public void countSubTrieNote(TrieNode p) {
		if (p.isEnd == true) {
			// System.out.println("plus 1!");
			cnt++;
		}
		TrieNode[] nodes = p.arr;
		for (int i = 0; i < nodes.length; i++) {
			TrieNode node = nodes[i];
			if (node != null) {
				countSubTrieNote(node);
			}
		}
		return;
	}

	// Returns if there is any word in the trie
	// that starts with the given prefix.
	// public boolean startsWith(String prefix) {
	// TrieNode p = searchNode(prefix);
	// if (p == null) {
	// return false;
	// } else {
	// return true;
	// }
	// }

	public TrieNode searchNode(String s) {
		TrieNode p = root;
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			int index = c - 'a';
			if (p.arr[index] != null) {
				p = p.arr[index];
			} else {
				return null;
			}
		}

		if (p == root)
			return null;
		// System.out.println(p.toString());
		return p;
	}

	public int lookupAndCount(String search_word) {
		// System.out.println("Look up: " + search_word);
		if (search(search_word)) {
			// System.out.println("yes");
		} else {
			// System.out.println("no");
		}
		int ans = this.cnt;
		System.out.println(ans);
		this.cnt = 0;
		return ans;
	}

	// https://www.hackerrank.com/challenges/ctci-contacts
	// Submit Reuslt: total 13 test -- 9 pass, 4 timeout
	public static void main(String[] args) {

		/*
		 * 4 add hack add hackerrank find hac find hak
		 */
		Hackerrank_Tries_Contacts arraytrieDictionary = new Hackerrank_Tries_Contacts();
		// arraytrieDictionary.allowPartialSearch = true;

		in = new Scanner(System.in);
		int n = in.nextInt(); // num of input elements

		for (int i = 0; i < n + 1; i++) {
			String instruction = in.nextLine();
			// System.out.println(instruction);
			if (instruction.length() >= 4) {
				if (instruction.substring(0, 3).equals("add")) {
					// System.out.println("adding " + instruction.substring(4));
					arraytrieDictionary.insert(instruction.substring(4));
				}
				if (instruction.substring(0, 4).equals("find")) {
					// System.out.println("looking " +
					// instruction.substring(5));
					arraytrieDictionary.lookupAndCount(instruction.substring(5));
				}
			}
		}

		System.exit(0);

		// arraytrieDictionary.insert("hackerrank");
		// arraytrieDictionary.insert("hack");
		//
		// arraytrieDictionary.lookupAndCount("hack");
		// arraytrieDictionary.lookupAndCount("hak");

	}




}

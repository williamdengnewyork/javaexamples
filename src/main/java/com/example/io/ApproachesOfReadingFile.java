package com.example.io;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class ApproachesOfReadingFile {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		List<List<String>> records = new ArrayList<List<String>>();
		double s_time = System.currentTimeMillis();
		// 50MB, 1MM records, text words
		String RecordFileName = "c:\\WordFrequency\\records.txt";
		System.out.println("Loading Records...");
		System.out.println("RecordsFile = " + Paths.get(RecordFileName));

		// 1. Create Data Structure List<List<String>> for Records
//		records = new ArrayList<List<String>>();
//		try {
//			for (String record : java.nio.file.Files.readAllLines(Paths.get(RecordFileName), StandardCharsets.UTF_8)) {
//				String[] recWords = record.split(",");
//				List<String> recWordsList = new ArrayList<String>(Arrays.asList(recWords));
//				records.add(recWordsList);
//			}
//		} catch (IOException e1) {
//			e1.printStackTrace();
//		}
//		System.out.println(">>> Total Records:  " + records.size() + " Loaded in "
//				+ (System.currentTimeMillis() - s_time) / 1000 + " Seconds --- by java.nio.file.Files.readAllLines \n");


		// 2. Java 8 BR -- Files.newBufferedReader == is stream
		records = new ArrayList<List<String>>();
		s_time = System.currentTimeMillis();
		try (BufferedReader br = Files.newBufferedReader(Paths.get(RecordFileName))) {
			// br returns as stream and convert it into a List
			br.lines().map(record -> {
				String[] recWords = record.split(",");
				return Arrays.asList(recWords);
			}).forEach(records::add);
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(">>> Total Records:  " + records.size() + " Loaded in "
				+ (System.currentTimeMillis() - s_time) / 1000 + " Seconds --- by Files.newBufferedReader () \n");

		// final FileChannel channel = new
		// FileInputStream(fileName).getChannel();
		// MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_ONLY,
		// 0, channel.size());
		//
		// // when finished
		// channel.close();

		// 3. File Stream -- Files.lines
		records = new ArrayList<List<String>>();
		s_time = System.currentTimeMillis();
		try {
			Stream<String> lines;
			lines = Files.lines(Paths.get(RecordFileName));
			lines.map(record -> {
				String[] recWords = record.split(",");
				return Arrays.asList(recWords);
			}).forEach(records::add);
			lines.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(">>> Total Records:  " + records.size() + " Loaded in "
				+ (System.currentTimeMillis() - s_time) / 1000 + " Seconds  --- by File Stream -- Files.lines  \n");

		// 4. FileChannel
		FileInputStream fin = null;
		FileChannel fchannel = null;
		try {
			// opens a file to read from the given location
			fin = new FileInputStream("C:/temp/test.txt");

			// returns FileChannel instance to read the file
			fchannel = fin.getChannel();

			// allocate byte buffer size
			ByteBuffer byteBuffer = ByteBuffer.allocate(512);

			while (fchannel.read(byteBuffer) > 0) {

				// limit is set to current position and position is set to zero
				byteBuffer.flip();

				while (byteBuffer.hasRemaining()) {
					char ch = (char) byteBuffer.get();
					System.out.print(ch);
				}

				// position is set to zero and limit is set to capacity to clear
				// the buffer.
				byteBuffer.clear();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fin.close();
				fchannel.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}

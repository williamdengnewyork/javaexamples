package com.example.valueobject;

public class Customer {

	private int customer_id;
	//private Date DOB;
	private String f_name;
	private String l_name;
	private String Addr1;
	private String Addr2;
	private String State;
	private String Country;
	
	public Customer(int customer_id, String f_name, String l_name) {
		super();
		this.customer_id = customer_id;
		this.f_name = f_name;
		this.l_name = l_name;
	}
	
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	public String getF_name() {
		return f_name;
	}
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}
	public String getL_name() {
		return l_name;
	}
	public void setL_name(String l_name) {
		this.l_name = l_name;
	}
	public String getAddr1() {
		return Addr1;
	}
	public void setAddr1(String addr1) {
		Addr1 = addr1;
	}
	public String getAddr2() {
		return Addr2;
	}
	public void setAddr2(String addr2) {
		Addr2 = addr2;
	}
	public String getState() {
		return State;
	}
	public void setState(String state) {
		State = state;
	}
	public String getCountry() {
		return Country;
	}
	public void setCountry(String country) {
		Country = country;
	}
	
	@Override
	public String toString(){
		return customer_id+":"+l_name+","+f_name;
	}
	

	
	
}

package com.example.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.example.valueobject.Customer;

public class SortObjectListByProperties {

	static SortObjectListByProperties sortObjectListByProperties = new SortObjectListByProperties();

	public static void main(String[] args) {

		List<Customer> custList = new ArrayList<Customer>();
		custList.add(new Customer(2, "Matthew", "Deng"));
		custList.add(new Customer(1, "William", "Deng"));
		custList.add(new Customer(6, "William", "Deng"));
		custList.add(new Customer(3, "Timmy", "Deng"));
		custList.add(new Customer(4, "Jenny", "Cheung"));

		System.out.println("Before sort -" + custList);
		//sortObjectListByProperties.order(custList);
		sortObjectListByProperties.orderByLambda(custList);
		System.out.println("After sort -" + custList);

	}

	// order by: 1. l_name, f_name, id
	// Overwrite  Comparator<Object>().compare(o1, o2) methods, return int (0, -1, 1) Compare Result int: 0- equal, -1- less, 1- larger 
	private void order(List<Customer> custList) {

		Collections.sort(custList, new Comparator<Object>() {

			// Compare Result int: 0- equal, -1- less, 1- larger 
			public int compare(final Object o1, final Object o2) {
				final String x1 = ((Customer) o1).getL_name();
				final String x2 = ((Customer) o2).getL_name();
				// order of compare is important
				final int LNComp = x1.compareToIgnoreCase(x2);
				if (LNComp != 0) {
					return LNComp;
				} else {
					final String f1 = ((Customer) o1).getF_name();
					final String f2 = ((Customer) o2).getF_name();
					// order of compare is important
					final int FNComp = f1.compareToIgnoreCase(f2);    // compare String - Case Insensitive
					if (FNComp != 0) {
						return FNComp;
					}
					final Integer id1 = ((Customer) o1).getCustomer_id();
					final Integer id2 = ((Customer) o2).getCustomer_id();
					// order of compare is important
					return id1.compareTo(id2);
				}
			}
		});
	}

	
	// order by: 1. l_name, f_name, id
	//Lambda Expression
	private void orderByLambda(List<Customer> customer) {
		//Lambda Expression (o1, o2) ->
		Collections.sort(customer, (o1, o2) -> {
			final String x1 = ((Customer) o1).getL_name();
			final String x2 = ((Customer) o2).getL_name();
			// order of compare is important
			final int LNComp = x1.compareTo(x2);			// compare String - Case Sensitive
			if (LNComp != 0) {
				return LNComp;
			} else {
				final String f1 = ((Customer) o1).getF_name();
				final String f2 = ((Customer) o2).getF_name();
				// order of compare is important
				final int FNComp = f1.compareTo(f2);
				if (FNComp != 0) {
					return FNComp;
				}
				final Integer id1 = ((Customer) o1).getCustomer_id();
				final Integer id2 = ((Customer) o2).getCustomer_id();
				// order of compare is important
				return id1.compareTo(id2);
			}
		});
	}
	
	
	
}

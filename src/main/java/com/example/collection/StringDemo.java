package com.example.collection;

import java.util.Scanner;

public class StringDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// Stripping the last , in a String series 
		String series = "aaa, bbb, ccc, ddd,      ";
		System.out.println(series + " - last index , is:" + series.trim().lastIndexOf(",")); 
		System.out.println("After stripping last , =" + series.substring(0, (series.lastIndexOf(","))) + "|***" );
	
//        Scanner sc=new Scanner(System.in);
//        String A=sc.next();
//        String B=sc.next();

		/* Enter your code here. Print output to STDOUT. */
        String A="hello";
        String B="Java";
		System.out.println(A.length() + B.length());
        
	}

}

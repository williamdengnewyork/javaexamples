package com.example.collection;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPPart;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;



public class SoapNavigation {

	/**
	 * @param args
	 * @throws SOAPException 
	 * @throws IOException 
	 * @throws XPathExpressionException 
	 */
	public static void main(String[] args) throws SOAPException, IOException, XPathExpressionException {
		// TODO Auto-generated method stub

		
		// Creating SOAP Message
		    SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
		    SOAPPart soapPart = soapMessage.getSOAPPart();
		    SOAPEnvelope soapEnvelope = soapPart.getEnvelope();

		    SOAPHeader soapHeader = soapEnvelope.getHeader();
		    Name headerbodyName = soapEnvelope.createName("Signature", "SOAP-SEC", "http://schemas.xmlsoap.org/soap/security/2000-12");
		    SOAPHeaderElement headerElement = soapHeader.addHeaderElement(headerbodyName);

		    SOAPBody soapBody = soapEnvelope.getBody();
		    soapBody.addAttribute(soapEnvelope.createName("id", "SOAP-SEC", "http://schemas.xmlsoap.org/soap/security/2000-12"), "Body");
		    Name bodyName = soapEnvelope.createName("FooBar", "z", "http://example.com");
		    SOAPBodyElement gltp = soapBody.addBodyElement(bodyName);

		    Source source = soapPart.getContent();
		    soapMessage.writeTo(System.out);
		    

//		    ByteArrayOutputStream out = new ByteArrayOutputStream();
//            soapMessage.writeTo(out);
//            String request_soapMessage = new String(out.toByteArray());
//            String response_soapMessage = "";

		    
		//Reading SOAP Message
		    //SOAPMessage message = connection.call(sm, endpoint);
		    XPath xpath = XPathFactory.newInstance().newXPath();
		    NamespaceContext context= (NamespaceContext)source ;
		    //new NamespaceContext();
		    xpath.setNamespaceContext(context);
		    Node resultNode = (Node) xpath.evaluate("//m:GetWhoISResult", soapMessage.getSOAPBody(), XPathConstants.NODE);
		    
		    
		    
		  }
		
		/*
		  SOAPMessage msg = new SOAPMessage();
		  msg.
	      SOAPBody soapBody = response.getSOAPBody();

	      Iterator iterator = 
	        soapBody.getChildElements(bodyName);
	      
	      soapBody.
	      bodyElement = (SOAPBodyElement)iterator.next();
	      String lastPrice = bodyElement.getValue();
	      
	      
          SOAPPart soapPart 	= soapMessage.getSOAPPart();
          SOAPEnvelope envelope = soapPart.getEnvelope();
          SOAPBody soapbody 	= soapMessage.getSOAPBody();
          
          for (Iterator iter = soapbody.getChildElements(); iter.hasNext();) {
          	SOAPBodyElement bodyElement = (SOAPBodyElement) iter.next();
              
          }
          */
	      
	

}

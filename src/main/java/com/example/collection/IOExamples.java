package com.example.collection;

/*
Programs use byte streams to perform input and output of 8-bit bytes. All byte stream classes are descended from InputStream and OutputStream.
There are many byte stream classes. To demonstrate how byte streams work, we'll focus on the file I/O byte streams, FileInputStream and FileOutputStream. Other kinds of byte streams are used in much the same way; they differ mainly in the way they are constructed.
Using Byte Streams
We'll explore FileInputStream and FileOutputStream by examining an example program named CopyBytes, which uses byte streams to copy xanadu.txt, one byte at a time.

Keys:
1. InputStream/FileInputStrem read() method reads from a file "byte-by-byte"
2. each "byte" is represent as the associated integer in ASCII.
3. Unbuffered - underlying OS handles read/write, in-efficient!
*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

//http://docs.oracle.com/javase/tutorial/essential/io/objectstreams.html
public class IOExamples {

	public static void main(String[] args) throws IOException {
		
		//Byte Stream
		//IOByteByByte();
	
		//Character Stream
		/*
		 * Both use an int variable to read to and write from.
		 * The most important difference:  
		 * CopyCharacters - FileReader and FileWriter 
		 * the int variable holds a character value in its last 16 bits - 2 Bytes; 
		 * CopyBytes - FileInputStream and FileOutputStream 
		 * the int variable holds a byte value in its last 8 bits - 1 Byte.
		 */
		//IOCharacterByCharacter();
		
		//Buffered Stream
		IOBuferedStream();

    }

    public static void IOBuferedStream() throws IOException{
    	BufferedReader inputStream = null;
    	BufferedWriter outputStream = null;

        try {
            inputStream = new BufferedReader(new FileReader("IMG_0080.jpg"));
            outputStream = new BufferedWriter(new FileWriter("BufferedOutputStream.txt"));
            
            int c;
            System.out.println("Start at :" + new java.util.Date());
            while ((c = inputStream.read()) != -1) {
            	//System.out.println("output: " + new Integer(c) );
                outputStream.write(c);
            }
            System.out.println("End at :" + new java.util.Date());            
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }
    
    public static void IOCharacterByCharacter() throws IOException{
    	
        FileReader inputStream = null;
        FileWriter outputStream = null;

        try {
            //inputStream = new FileReader("FileInputStream.txt");
            inputStream = new FileReader("IMG_0080.jpg");
            outputStream = new FileWriter("FileCharacterOutputStream.txt");

            int c;
            while ((c = inputStream.read()) != -1) {
            	System.out.println("output: " + new Integer(c) );
                outputStream.write(c);
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (outputStream != null) {
                outputStream.close();
            }
        }

    }
	
	
    public static void IOByteByByte() throws IOException{
    
        FileInputStream in = null;
        FileOutputStream out = null;
        try {
            //in = new FileInputStream("C:\\EclipseWorkspace\\JavaLangExamples\\src\\sample_input.txt");
            //in = new FileInputStream("FileInputStream.txt");
            in = new FileInputStream("IMG_0080.jpg");
            out = new FileOutputStream("FileOutputStream.txt");
            int c;

            while ((c = in.read()) != -1) {
            	System.out.println("output: " + new Integer(c) );
                out.write(c);
            }

        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }
    }
    
    // NIO.File - Read contents of a file to String
	public static  String readFromFile(String fileName) throws IOException, URISyntaxException {
		StringBuilder sb = new StringBuilder();
		System.out.println("Paths.get(fileName)=" + Paths.get(fileName));
		
		for (String line : java.nio.file.Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8)) {
			sb.append(line).append("\n");
		}
	return sb.toString();
}
    
    
}

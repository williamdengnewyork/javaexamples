package com.example.collection;

public class Account {
	private int acct_id;
	private String acct_name;
	private String status;
	
	public Account(int acct_id, String acct_name, String status) {
		super();
		this.acct_id = acct_id;
		this.acct_name = acct_name;
		this.status = status;
	}
	
	public int getAcct_id() {
		return acct_id;
	}
	public void setAcct_id(int acct_id) {
		this.acct_id = acct_id;
	}
	public String getAcct_name() {
		return acct_name;
	}
	public void setAcct_name(String acct_name) {
		this.acct_name = acct_name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString(){
		return acct_id+"-"+acct_name;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + acct_id;
		result = prime * result
				+ ((acct_name == null) ? 0 : acct_name.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (acct_id != other.acct_id)
			return false;
		if (acct_name == null) {
			if (other.acct_name != null)
				return false;
		} else if (!acct_name.equals(other.acct_name))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}

}

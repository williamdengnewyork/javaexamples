package com.example.collection;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.example.valueobject.Customer;


public class HashMapDemo {

	public static void main(String[] args) {

		HashMap<Integer, Customer> CustomerMap = new HashMap<Integer, Customer>();		
		Customer cust1 = new Customer(1,"William", "Deng");

		CustomerMap.put(new Integer(1), cust1);
		CustomerMap.put(new Integer(2), new Customer(2,"Billy", "Joe"));
		CustomerMap.put(new Integer(3), new Customer(3,"Tom", "Li"));
		CustomerMap.put(new Integer(4), new Customer(4,"Rich", "Chong"));

		// Loop through HashMap entry with Iterator
		Iterator<Entry<Integer, Customer>> iter = CustomerMap.entrySet().iterator();
		while(iter.hasNext()) {
			Map.Entry<Integer, Customer> pairs = (Map.Entry<Integer, Customer>) iter.next();			        
			System.out.println(pairs.getKey() + " = " + pairs.getValue());
		}
	
		//Search/Lookup HashMap
		if (CustomerMap.containsKey(new Integer(3))){
			System.out.println ("HashMap has Key of Integer 3");
		}
		
		if (CustomerMap.containsValue(cust1)){
			System.out.println ("HashMap has Entry of customer William Deng ");
		}
		
		
		////////////////// <<HashMap>> ////////////////////////
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("William Deng", "668-1238");
		hm.put("Jenny Deng", "916-3039");
		
		// initial a Hash Map
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("1", "Jan");
		map.put("2", "Feb");
		map.put("3", "Mar");
		map.put("4", "Apr");
		map.put("5", "May");
		map.put("6", "Jun");
	 
		System.out.println("Hash Map Iteration Example 1...Iterator of entrySet");
		// Map -> Set -> Iterator -> Map.Entry -> troublesome?
		// Though old style, this helps avoid ConcurrentModificationExceptions over the new foreach style in the answers below. 
		Iterator iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry mapEntry = (Map.Entry) iterator.next();
			System.out.println("The key is: " + mapEntry.getKey()+ ",value is :" + mapEntry.getValue());
			//iterator.remove(); // If need to modify, avoids a ConcurrentModificationException
		}
	 
		System.out.println("Hash Map Iteration Example 2... Direct ForLoop of entrySet");
		// more elegant way, more efficient
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			System.out.println("Key : " + entry.getKey() + " Value : "	+ entry.getValue());
		}
	 
		System.out.println("Hash Map Iteration Example 3...");
		// weired way, but works anyway
		for (Object key : map.keySet()) {
			System.out.println("Key : " + key.toString() + " Value : " 	+ map.get(key));
		}
		
		for (Map.Entry<String, Object> entry : map.entrySet()) {
		    String key = entry.getKey();
		    Object value = entry.getValue();
		}
		
	}
			
}

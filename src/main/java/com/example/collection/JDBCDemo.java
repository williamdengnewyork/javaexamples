package com.example.collection;

/*
1/8/2010
William Deng
*/

import java.sql.*;
import java.io.*;

public class JDBCDemo  {

    private  static String driver= null;
    private  static String dburl= null;
    private  static String username= null;
    private  static String password= null;
    private static Connection con = null;
    private static Statement stmt = null;


   /**
 * @param args
 * @throws SQLException
 * @throws ClassNotFoundException
 * @throws IllegalAccessException
 * @throws InstantiationException
 */
public static void main(String[] args) throws SQLException,ClassNotFoundException,
    IllegalAccessException, InstantiationException {

		try {

		setConnectionProperties("sybase");	
			
		//1. Load DB Vendor Driver to register it
//        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
        Class.forName(driver);
          // OR
          // System.setProperty(jdbc:drivers", driver);
          // System.setProperty(jdbc:drivers", driver+":"+driver2+";"driver3); //load multiple drivers

		/* //Useful Debugging feature for troublshooting detail.
 		this method checks to see that there is an SQLPermission object before setting
		the logging stream. If a SecurityManager exists and its checkPermission method
		denies setting the log writer, this method throws a java.lang.SecurityException.
		*/
		DriverManager.setLogWriter(new PrintWriter("jdbcapp.log"));

		//2. Connection
		con = DriverManager.getConnection(dburl,username,password);

		/*
		JDBC supports three types of statements:
			1.Statement
			2.PreparedStatement
			3.CallableStatement
		*/

        DemoStatement();

        //DemoPreparedStatement();

        //DemoCallableStatement();

		} catch (ClassNotFoundException e) {
	            e.printStackTrace();
		} catch (FileNotFoundException e) {
	            e.printStackTrace();
		} catch (SQLException sqle) {

       	  while (sqle !=null)  // SQLException may be multiplely chained
      	  {// Catch detail of SQLException error
          String jdbcmsg	=sqle.getMessage();
          String sqlState 	=sqle.getSQLState();  //5-digit string error code
  		  int sqlerrcode 	=sqle.getErrorCode();

		  System.out.println("JDBC error message: "+jdbcmsg);
		  System.out.println("XOPEN/SQL 99 convention state msg:"+sqlState); //40001 for dealock
		  System.out.println("Database vendor(Sybase, Oracle) error code: "+sqlerrcode);
		  sqle.printStackTrace();
	  	  sqle = sqle.getNextException();
	  	  }
	    }
		finally{
		  if(con != null)
		    try{
		      con.close();
			} catch (Exception e){}
		}
	}

   private static void setConnectionProperties(String type)
   {
	   if (type.equals("mssql"))
	   {
		    driver="com.microsoft.sqlserver.jdbc.SQLServerDriver";
		    dburl="jdbc:sqlserver://DELL510;databaseName=testdb";
		    username="williamd";
		    password="vacation";		   
	   }
	   
	   if (type.equals("oracle"))
	   {	// Oracel 10g
		    driver="oracle.jdbc.driver.OracleDriver";
		    //dburl="jdbc:oracle:thin:@localhost:1521:orcl";
		    //dburl="jdbc:oracle:thin:@LNNY4NY3YHSLC1.NAEAST.AD.JPMORGANCHASE.COM:1521:orcl";
		    dburl="jdbc:oracle:thin:@jdbc:oracle:thin:@tsnc5ldbod350-scan.svr.us.jpmchase.net:1535:CSWT1";
		    username="csw";  // "SYSTEM"
		    password="csw2010";		   
	   }
	   
	   if (type.equals("sybase"))
	   {	
		    driver="com.sybase.jdbc3.jdbc.SybDriver";
		    dburl="jdbc:sybase:Tds:sybuat1xmn.jpmchase.net:7570";
		    username="PMRSSYS"; 
		    password="pmrs1001";		   
	   }
   }

	private static void DemoStatement() throws SQLException {
		try{
		// Create a simple Statement object.
		// rs will be scrollable, will not show changes made by others,
        // and will be updatable

		Statement stmt = con.createStatement();
		
		//Statement configuration
		stmt.setQueryTimeout(30);
		stmt.setMaxRows(1000);
		stmt.setMaxFieldSize(256);

		// Make a SQL Update :stmt.executeUpdate( update_string);
		String update_string = "UPDATE Customer SET lname='Cheung' WHERE ID=2" ;
		int RetValue = stmt.executeUpdate( update_string);
		System.out.println("Rows Updated " + RetValue );
		con.commit();

		// Make a SQL Select; stmt.executeQuery( query_string);
		String query_string = "Select ID, fname, lname from Customer ";
		ResultSet rset = stmt.executeQuery( query_string);

		System.out.println( " [ID]   " +  " [fname] "+ "  [lname] " );
		while (rset.next()){
			// Get the ResultSet by column number or by column name
			System.out.println( rset.getInt(1) +"       " + rset.getString(2) +"       "+ rset.getString(3));
		}

		
		stmt.clearBatch();

		} catch (SQLException sqle) {
		          // Catch detail of SQLException error
		          String jdbcmsg	=sqle.getMessage();
		          String sqlState 	=sqle.getSQLState();
		  		  int sqlerrcode 	=sqle.getErrorCode();

				  System.out.println("JDBC error message: "+jdbcmsg);
				  System.out.println("XOPEN/SQL 99 convention state msg:"+sqlState); //40001 for dealock
				  System.out.println("Database vendor(Sybase, Oracle) error code: "+sqlerrcode);
	    }finally{
		  if(stmt != null)
		    try{
		      stmt.close();
			} catch (Exception e){}
		}
	}


	private static void DemoPreparedStatement() throws SQLException {
		try{
		// Create a prepared Statement object, with "?" as input parameters
		// Query passes to SQL server, quary plan created, return back handle PrepStmt
		PreparedStatement PrepStmt = con.prepareStatement("SELECT ClassName, Location, DaysAndTimes FROM Classes WHERE ClassName = ?");
		// Can also prepare for SP call
		//ps = _con.prepareStatement("exec ctq_export_month ?");

		// Set the SQL parameter to the one passed into this method
		PrepStmt.setString(1,"Math");
		ResultSet rs_ret1 = PrepStmt.executeQuery();

		// Save time for similar querys
		PrepStmt.setString(1,"English");
		ResultSet re_ret2 = PrepStmt.executeQuery();  //No more SQL server preparation, quicker

		} catch (SQLException sqle) {
		          // Catch detail of SQLException error
		          String jdbcmsg	=sqle.getMessage();
		          String sqlState 	=sqle.getSQLState();
		  		  int sqlerrcode 	=sqle.getErrorCode();

				  System.out.println("JDBC error message: "+jdbcmsg);
				  System.out.println("XOPEN/SQL 99 convention state msg:"+sqlState); //40001 for dealock
				  System.out.println("Database vendor(Sybase, Oracle) error code: "+sqlerrcode);
	    }finally{
		  if(stmt != null)
		    try{
		      stmt.close();
			} catch (Exception e){}
		}

	}


	private static void DemoCallableStatement() throws SQLException {
		try{
		// For SP calls: e.g. studentGrade = (SP)getStudentGrade(StudentID,ClassID)
		//Create a Callable Statement object.
		CallableStatement CStmt = con.prepareCall("?={call getStudentGrade[?,?]}");

		// Now tie the placeholders with actual parameters.
		// Register the return value from the stored procedure
		// as an integer type so that the driver knows how to handle it.
		// Note the type is defined in the java.sql.Types.
		CStmt.registerOutParameter(1,java.sql.Types.INTEGER);

		// Set the In parameters (which are inherited from the PreparedStatement class)
		CStmt.setString(1,"322320");  //StudentID
		CStmt.setString(2,"21");		//ClassID
		// call the stored procedure
		int RetVal = CStmt.executeUpdate();
		// Get the OUT parameter from the registered parameter
		// Note that we get the result from the CallableStatement object
		int Grade = CStmt.getInt(1);

		} catch (SQLException sqle) {
		          // Catch detail of SQLException error
		          String jdbcmsg	=sqle.getMessage();
		          String sqlState 	=sqle.getSQLState();
		  		  int sqlerrcode 	=sqle.getErrorCode();

				  System.out.println("JDBC error message: "+jdbcmsg);
				  System.out.println("XOPEN/SQL 99 convention state msg:"+sqlState); //40001 for dealock
				  System.out.println("Database vendor(Sybase, Oracle) error code: "+sqlerrcode);
	    }finally{
		  if(stmt != null)
		    try{
		      stmt.close();
			} catch (Exception e){}
		}


	}


}

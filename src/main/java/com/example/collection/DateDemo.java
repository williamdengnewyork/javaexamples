package com.example.collection;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateDemo {

	/**
	 * Convert java.utl.Date to String
	 */
	public static void main(String[] args) {
		// Create an instance of SimpleDateFormat used for formatting 
		// the string representation of date (month/day/year)
		//DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");

		// Get the date today using Calendar object.
		Date today = Calendar.getInstance().getTime();        
		// Using DateFormat format method we can create a string 
		// representation of a date with the defined format.
		String todayDate = df.format(today);

		// Print what date is today!
		System.out.println("Report Date = " + todayDate);
		

//		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
//		String todayDate = df.format(Calendar.getInstance().getTime());
//		System.out.println("Report Date = " + todayDate);

	}

}

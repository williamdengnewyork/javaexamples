
package com.example.collection;

import java.util.UUID;

public class UUIDExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
        //
        // Creating a random UUID (Universally unique identifier).
        //
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();

        System.out.println("Random UUID String = " + randomUUIDString);
        System.out.println("Random UUID MostSignificantBits String = " + UUID.randomUUID().getMostSignificantBits() );
        System.out.println("UUID version       = " + uuid.version());
        System.out.println("UUID variant       = " + uuid.variant());

		String mystring = "fawfoijoeagvjadogv<app_id>CSW</app_id>gsdgbsgsdrgsdg";
		System.out.println("position: " + mystring.indexOf("<app_id>", 0));
		System.out.println("position: " + mystring.indexOf("</app_id>", 0));
		System.out.println("app_id=" + mystring.substring( mystring.indexOf("<app_id>", 0)+ "<app_id>".length(), mystring.indexOf("</app_id>", 0)));
	}


}

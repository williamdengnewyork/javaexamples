package com.example.collection;

/*
 *     file: FinalCollections.java
 *  package: oreilly.hcj.finalstory
 *
 * This software is granted under the terms of the Common Public License,
 * CPL, which may be found at the following URL:
 * http://www-124.ibm.com/developerworks/oss/CPLv1.0.htm
 *
 * Copyright(c) 2003-2005 by the authors indicated in the @author tags.
 * All Rights are Reserved by the various authors.
 *
 ########## DO NOT EDIT ABOVE THIS LINE ########## */

import java.awt.Color;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Demonstrates the use of final collections.
 * 
 * @author <a href=mailto:kraythe@arcor.de>Robert Simmons jr. (kraythe)</a>
 * @version $Revision: 1.3 $
 */
public class FinalCollections {
  /**
   * Main method for demonstration.
   * 
   * @param args
   *          Command line arguments.
   */
  public static final void main(final String[] args) {
    Rainbow.someMethod();
    System.out.println("------------------");
    try {
      // demo here!
    	RainbowBetter.someMethod();
    } catch (final UnsupportedOperationException ex) {
      ex.printStackTrace(System.out);
      System.out.println("Told you it would exception =)");
    }
  }

  /**
   * A class that demonstrates the rainbow.
   * 
   * @author <a href=mailto:kraythe@arcor.de>Robert Simmons jr. (kraythe)</a>
   * @version $Revision: 1.3 $
   */
  public static class Rainbow {
    /** The set of valid colors of the rainbow. */
    public static final Set VALID_COLORS;

    static {
      VALID_COLORS = new HashSet();
      VALID_COLORS.add(Color.red);
      VALID_COLORS.add(Color.orange);
      VALID_COLORS.add(Color.yellow);
      VALID_COLORS.add(Color.green);
      VALID_COLORS.add(Color.blue);
      VALID_COLORS.add(Color.decode("#4B0082")); // indigo
      VALID_COLORS.add(Color.decode("#8A2BE2")); // violet
    }

    /**
     * A demo method.
     */
    public static final void someMethod() {
      Set colors = Rainbow.VALID_COLORS;
      colors.add(Color.black); // <= logic error but allowed by compiler
      System.out.println(colors);
    }
  }

  /**
   * A better version of the rainbow class using an unmodifiable set.
   * 
   * @author <a href=mailto:kraythe@arcor.de>Robert Simmons jr. (kraythe)</a>
   * @version $Revision: 1.3 $
   */
  public static class RainbowBetter {
    /** The valid colors of the rainbow. */
    public static final Set VALID_COLORS;
    //public static  Set VALID_COLORS;

    static {
      Set temp = new HashSet();
      temp.add(Color.red);
      temp.add(Color.orange);
      temp.add(Color.yellow);
      temp.add(Color.green);
      temp.add(Color.blue);
      temp.add(Color.decode("#4B0082")); // indigo
      temp.add(Color.decode("#8A2BE2")); // violet
      // This line Collections.unmodifiableSet prohibits further modification
      //VALID_COLORS = Collections.unmodifiableSet(temp);
      VALID_COLORS = temp;    // This allows modifications
    }

    /**
     * Some demo method.
     */
    public static final void someMethod() {
      Set colors = RainbowBetter.VALID_COLORS;
      colors.add(Color.black); // <= exception here: java.lang.UnsupportedOperationException
      System.out.println(colors);
    }
  }
}

/* ########## End of File ########## */
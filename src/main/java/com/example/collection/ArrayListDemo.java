package com.example.collection;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayListDemo {

	public static void main(String[] a) {
		ArrayList<String> numberList = new ArrayList<String>();
		numberList.add("One");
		numberList.add("Two");
		numberList.add("Three");
		numberList.add("Four");

		// Loop through ArrayList,  index i
		int totalElements = numberList.size();
		for (int index = 0; index < totalElements; index++){
			System.out.println(numberList.get(index));
		}

		
		// Loop through Iterator, without index i
		String s;
		Iterator iterator = numberList.iterator();
		while (iterator.hasNext()) {
			s = (String) iterator.next();
			System.out.println(s);
		}

		for (iterator = numberList.iterator(); iterator.hasNext();) {
			String element = (String) iterator.next();
			System.out.println(element);
		}

		for (String element : numberList) {
			System.out.println(element);
		}


		//Add and Remove
		numberList.remove(new String("Three"));

		//Search by object containing 
		if (numberList.contains(new String("Three")))
			System.out.println("It has Three!");
		else 
			System.out.println("It DOES NOT have Three!");

		// Search by index
		System.out.println("The 3rd element: " + numberList.get(2));


	}

	
}

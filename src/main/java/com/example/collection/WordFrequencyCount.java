package com.example.collection;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordFrequencyCount {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {

		double s_time = System.currentTimeMillis();
		String RecordFileName = "c:\\WordFrequency\\records.txt";
		String QueryFileName = "c:\\WordFrequency\\queries.txt";
		String OutputFileName = "c:\\WordFrequency\\output.txt";

		System.out.println("RecordsFile = " + Paths.get(RecordFileName));
		System.out.println("QueriesFile = " + Paths.get(QueryFileName));
		System.out.println("OutputFile = " + Paths.get(OutputFileName));

		// 1. Create Data Structure List<List<String>> for Records
		System.out.println("Loading Records...");
		List<List<String>> records = new ArrayList<List<String>>();

		// Java 8 BR
		// try (BufferedReader br =
		// Files.newBufferedReader(Paths.get(RecordFileName))) {
		// // br returns as stream and convert it into a List
		// br.lines().map(record -> {
		// String[] recWords = record.split(",");
		// return Arrays.asList(recWords);
		// }).forEach(records::add);
		//
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// System.out.println(">>> Total Records: " + records.size() + " Loaded
		// in "
		// + (System.currentTimeMillis() - s_time) / 1000 + " Seconds --- bR");

		records = new ArrayList<List<String>>();
		// Java 8 File Stream
		Stream<String> lines = Files.lines(Paths.get(RecordFileName));
		lines.map(record -> {
			String[] recWords = record.split(",");
			return Arrays.asList(recWords);
		}).forEach(records::add);
		lines.close();

		System.out.println(">>> Total Records:  " + records.size() + " Loaded in "
				+ (System.currentTimeMillis() - s_time) / 1000 + " Seconds  \n");

		// 2. Get new words per Query
		File outputFile = new File(OutputFileName);

		try (FileOutputStream fop = new FileOutputStream(new File(OutputFileName))) {
			// if file doesn't exists, then create it
			if (!outputFile.exists()) {
				outputFile.createNewFile();
			}
			String outputline = new String();

			for (String query : java.nio.file.Files.readAllLines(Paths.get(QueryFileName), StandardCharsets.UTF_8)) {
				String[] queryWords = query.split(",");
				List<String> queryWordsList = new ArrayList<String>(Arrays.asList(queryWords));
				System.out.println(">>> Query Words: " + queryWordsList);

				// a) get new words per Query by Java 8 Stream, Lambda
				// expression
				List<String> queryNewWords = records.stream().filter(lst -> lst.containsAll(queryWordsList))
						.flatMap(lst -> lst.stream()).collect(Collectors.toList());
				queryNewWords.removeAll(queryWordsList);
				System.out.println("Total New Words for the Query:" + queryNewWords);

				// b) get counts of each new words
				Map<String, Long> map = queryNewWords.stream()
						.collect(Collectors.groupingBy(m -> m, Collectors.counting()));

				// c) format results for output
				TreeMap<String, Long> treeMap = new TreeMap<String, Long>(map);
				StringBuilder sb = new StringBuilder();
				for (Entry<String, Long> entry : treeMap.entrySet()) {
					sb.append("\"" + entry.getKey() + "\":" + " " + entry.getValue() + ", ");
				}
				outputline = "{" + sb.toString().substring(0, sb.toString().length() - 2) + "} \n";
				fop.write(outputline.getBytes());
				System.out.println(">>> Counts of New Words: " + outputline);
			}
			
			fop.flush();
			fop.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		double total_time = System.currentTimeMillis() - s_time;
		System.out.println(">>> Job took " + total_time / 1000 + " Seconds to complete.");

	}

}

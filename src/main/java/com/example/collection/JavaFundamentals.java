package com.example.collection;
public class JavaFundamentals {

 public static void main (String[] args) {
	 JavaFundamentals jf= new JavaFundamentals();
 	jf.printClassName(jf);
 }

 public void printClassName(Object obj) {
         System.out.println("The class of " + obj +
                            " is " + obj.getClass().getName());
     }

}
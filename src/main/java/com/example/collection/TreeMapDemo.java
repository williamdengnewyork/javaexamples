package com.example.collection;

import java.util.List;
import java.util.TreeMap;

public class TreeMapDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		TreeMap<String, List<Integer>> tm = new TreeMap<String, List<Integer>>();
		tm.put("william", new List(new Integer.valueOf(1)));
		tm.put("timmy", 2);

		System.out.println(tm);
		
	}

}

package com.example.collection;

import java.util.StringTokenizer;

public class StringTokenizerDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String input_str = "HFGAPBWG9,HFGAPBAT5,HFGAPBAN8" ;
		
		StringTokenizer st = new StringTokenizer(input_str, ",");
		int cnt = 0; 
		while (st.hasMoreElements()) {
			System.out.println(st.nextElement());
			cnt += 1;
		}
		System.out.println("total: " + cnt);
	}

}

package com.example.collection;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CollectionDemo {


	  public static void main(String[] args) {
	    final LinkedList<String> myQueue = new LinkedList<String>();
	    myQueue.add("A");
	    myQueue.add("B");
	    myQueue.add("C");
	    myQueue.add("D");

	    final List<String> myList = new ArrayList<String>(myQueue);

	    for (Object theFruit : myList)
	      System.out.println(theFruit);
	}
	/*
	A
	B
	C
	D
	*/
}

package com.example.collection;

public class SQLDateTimeTimeStamp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        
        java.util.Date date = new java.util.Date();
        long t = date.getTime();
        
        java.sql.Date sqlDate = new java.sql.Date(t);
        java.sql.Time sqlTime = new java.sql.Time(t);
        java.sql.Timestamp sqltimestamp = new java.sql.Timestamp(t);


        System.out.println("Java Util Date = " + date );
        System.out.println("Long value of Util Date = " + t );

        System.out.println("Java SQL Date = " + sqlDate );
        System.out.println("Java SQL Time = " + sqlTime );
        System.out.println("Java SQL Timestamp = " + sqltimestamp );
/*
        insert
        into
            CIM_REQUEST_RESPONSE_AUDIT
            (audit_id, app_id, audit_date, audit_user, relationship_id, request_xml,
     response_xml, status)
        values
            ( '2ba88e81-2d92-4f4e-838d-a34845d4c44e', 'CSW', to_timestamp('2011-10-18 11:
    47:25.148', 'YYYY-MM-DD HH:MI:SS:FF'), 'v303912', '123456667', hextoraw('453d7a34'), hextoraw
    ('453d7a34'), '0' )
  */  
	}

}

package com.example.collection;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ArrayListCompare {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		List<String> a = new ArrayList<String>();
		a.add("A");
		a.add("C");
		a.add("D");
		a.add("B");
		a.add("D");

		List<String> b = new ArrayList<String>();
		b.add("B");
		// b.add("F");
		b.add("A");

		System.out.println("a contains b = " + a.contains(b));
		System.out.println("a contains All b = " + a.containsAll(b));

		a.removeAll(b);
		System.out.println(a);

		//

		String RecordFileName = "c:\\WordFrequency\\records.txt";
		// String RecordFileName = "c:\\WordFrequency\\queries.txt";
		String QueryFileName = "c:\\WordFrequency\\queries.txt";

		System.out.println("Paths.get(RecordFileName)=" + Paths.get(RecordFileName));
		System.out.println("Paths.get(QueryFileName)=" + Paths.get(QueryFileName));

		List<List<String>> records = new ArrayList<List<String>>();

		// 1. Create Data Structure for Records
		for (String record : java.nio.file.Files.readAllLines(Paths.get(RecordFileName), StandardCharsets.UTF_8)) {
			// System.out.println(record);

			String[] recWords = record.split(",");
			List<String> recWordsList = new ArrayList<String>(Arrays.asList(recWords));
			// System.out.println(recWordsList);
			records.add(recWordsList);
		}
		System.out.println("Total Records Size: " + records.size());

		// 2. Query
		for (String query : java.nio.file.Files.readAllLines(Paths.get(QueryFileName), StandardCharsets.UTF_8)) {
			// System.out.println(query);

			String[] queryWords = query.split(",");
			List<String> queryWordsList = new ArrayList<String>(Arrays.asList(queryWords));
			System.out.println("Query List: " + queryWordsList);

			List<String> queryNewWords = new ArrayList<String>();
			for (int i = 0; i < records.size(); i++) {
				List<String> recWordsList = records.get(i);
				if (recWordsList.containsAll(queryWordsList)) { // relevant
																// record
					System.out.println("Relevant Record: " + recWordsList);

					recWordsList.removeAll(queryWordsList);
					System.out.println("Net New Words: " + recWordsList);

					queryNewWords.addAll(recWordsList);
				}

			}
			System.out.println("Total Query New Words for Query:" + queryNewWords);
			// JDK 7
			Set<String> mySet = new HashSet<String>(queryNewWords);
			for (String s : mySet) {
				System.out.println(s + " " + Collections.frequency(queryNewWords, s));
			}

			// JDK 8 - stream
			/*
			 * What it does is:
			 * 
			 * Create a Stream<String> from the original array Group each
			 * element by identity, resulting in a Map<String, List<String>> For
			 * each key value pair, print the key and the size of the list If
			 * you want to get a Map that contains the number of occurences for
			 * each word, it can be done doing:
			 */
			// Map<String, Long> map =
			// Arrays.stream(array).collect(Collectors.groupingBy(s -> s,
			// Collectors.counting()));

			System.out.println("-----------------------------------------");

		}

	}

}

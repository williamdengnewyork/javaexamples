package com.example.collection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

public class TestAnnotations {

	   private String str;

	  public static void main(String arg[]) {
	      new TestAnnotations().doTestTarget();
	   }
	   
	   @Test_Target(doTestTarget="Hello World !")
	   public void doTestTarget() {
	      System.out.printf("Testing Target annotation");
	   }

	   @Target(ElementType.METHOD)
	   public @interface Test_Target {
	      public String doTestTarget();
	   }



}

//	The @Target(ElementType.METHOD) indicates that this annotation type can be used to annotate only at the method levels. If you compile the preceding code, no warning messages will be shown. Now, if you declare a String variable and apply your newly created annotation, what will happen? Let me demonstrate this as follows:
/*
	public class TestAnnotations {
	
		@Test_Target(doTestTarget="Hello World !")
	   private String str;
	
	   
	   public static void main(String arg[]) {
	      new TestAnnotations().doTestTarget();
	   }
	   public void doTestTarget() {
	      System.out.printf("Testing Target annotation");
	   }
	}
*/

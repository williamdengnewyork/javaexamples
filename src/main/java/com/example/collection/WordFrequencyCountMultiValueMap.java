package com.example.collection;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

public class WordFrequencyCountMultiValueMap {

	public static void main(String[] args) throws IOException {

		double s_time = System.currentTimeMillis();
		String RecordFileName = "c:\\WordFrequency\\records.txt";
		// String RecordFileName = "c:\\WordFrequency\\queries.txt";
		String QueryFileName = "c:\\WordFrequency\\queries.txt";

		System.out.println("RecordsFile = " + Paths.get(RecordFileName));
		System.out.println("QueriesFile = " + Paths.get(QueryFileName));

		// 0. crete word frequency
		List<String> WordsList = new ArrayList<String>();
		for (String record : java.nio.file.Files.readAllLines(Paths.get(RecordFileName), StandardCharsets.UTF_8)) {
			String[] recWords = record.split(",");
			WordsList.addAll(new ArrayList<String>(Arrays.asList(recWords)));
		}
		System.out.println(">>> Word Frequecy Loaded: " + WordsList.size() + " in "
				+ (System.currentTimeMillis() - s_time) / 1000 + " Seconds");
		Map<String, Long> map1 = Arrays.stream(WordsList.toArray(new String[WordsList.size()]))
				.collect(Collectors.groupingBy(s -> s, Collectors.counting()));
		Map<String, Long> treeMap1 = new TreeMap<String, Long>(map1);
		System.out.println(treeMap1.size());
		System.out.println(treeMap1);

		// 1. Create Data Structure for Records
		ArrayListValuedHashMap<String, Integer> multiValueMap = new ArrayListValuedHashMap<String, Integer>();
		// HashSetValuedHashMap<String, Integer> multiValueMap = new
		// HashSetValuedHashMap<String, Integer>();
		List<List<String>> records = new ArrayList<List<String>>();
		int line = 0;
		for (String record : java.nio.file.Files.readAllLines(Paths.get(RecordFileName), StandardCharsets.UTF_8)) {
			String[] recWords = record.split(",");
			List<String> recWordsList = new ArrayList<String>(Arrays.asList(recWords));

			// a - Record List
			records.add(recWordsList);

			// b - multi-key Map (word, record#)
			for (String word : recWordsList) {
				multiValueMap.put(word, new Integer(line));
			}
			line++;
		}
		// Collections.sort(list);
		multiValueMap.asMap();
		System.out.println(">>> Total Records Loaded: " + records.size() + " in "
				+ (System.currentTimeMillis() - s_time) / 1000 + " Seconds");
		System.out.println(">>> Total Records Loaded: " + multiValueMap.size() + " in "
				+ (System.currentTimeMillis() - s_time) / 1000 + " Seconds");
		Map<String, Collection<Integer>> wordMap = new TreeMap<String, Collection<Integer>>();
		wordMap = multiValueMap.asMap();
		System.out.println(">>> Total Records Loaded: " + wordMap.size() + " in "
				+ (System.currentTimeMillis() - s_time) / 1000 + " Seconds");
		System.out.println("-----------------------------------------");

		// 2. Query
		for (String query : java.nio.file.Files.readAllLines(Paths.get(QueryFileName), StandardCharsets.UTF_8)) {
			String[] queryWords = query.split(",");
			List<String> queryWordsList = new ArrayList<String>(Arrays.asList(queryWords));
			System.out.println("Query Words: " + queryWordsList);

			List<String> queryNewWords = new ArrayList<String>();
			List<List<Integer>> candidateRecordsList = new ArrayList<List<Integer>>();

			for (String word : queryWordsList) {
				// candidateRecordsList.add(multiValueMap.get(word));
				// boolean add =
				// candidateRecordsList.add(multiValueMap.get(word));
				boolean add = candidateRecordsList.add((List<Integer>) wordMap.get(word));

			}

			List<Integer> relevantRecordsBase = candidateRecordsList.get(0);
			for (List<Integer> candidateRecords : candidateRecordsList) {
				relevantRecordsBase.retainAll(candidateRecords);
			}
			System.out.println(relevantRecordsBase);

			for (Integer rec_number : relevantRecordsBase) {
				queryNewWords.addAll(records.get(rec_number.intValue()));
			}
			queryNewWords.removeAll(queryWordsList);
			System.out.println("Total New Words for the Query:" + queryNewWords);

			// JDK 8 - Stream
			Map<String, Long> map = Arrays.stream(queryNewWords.toArray(new String[queryNewWords.size()]))
					.collect(Collectors.groupingBy(s -> s, Collectors.counting()));
			Map<String, Long> treeMap = new TreeMap<String, Long>(map);
			StringBuilder sb8 = new StringBuilder();
			for (Entry<String, Long> entry : treeMap.entrySet()) {
				sb8.append("\"" + entry.getKey() + "\":" + " " + entry.getValue() + ", ");
			}
			System.out.println(
					"Java 8 Streaming Output : " + "{" + sb8.toString().substring(0, sb8.toString().length()) + "}");

			System.out.println("-----------------------------------------");

		}

		double total_time = System.currentTimeMillis() - s_time;
		System.out.println(">>> Job took " + total_time / 1000 + " Seconds to complete.");

	}

}

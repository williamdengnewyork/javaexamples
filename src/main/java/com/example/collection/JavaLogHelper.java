package com.example.collection;

import java.util.logging.Logger;
import java.util.logging.LogManager;
import java.util.logging.Level;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * A simple wrapper around JDK logging facilities. The aim is to allow 
 * easy switch to another logging system, such as Log4J.
 */
 
public final class JavaLogHelper {

	//Instance of the Java logger object.
	  private final java.util.logging.Logger javaLogger;
	  private String properties_file="/apps/deploy/sao/properties/aps/logging.properties";  

	  /**  Constructor
	   * Create a new Logger Helper instance. 
	   * @param name 
	   * Name of the logger Helper, package names are recommended
	   */
	  /*
	   * Flexible Cofiguration Logger properties 
	   * 1. By default, use JVM logging properties files 
	   * 2. Caller can override JVM settings by giving valid properties  
	   * 3. if both (1) and (2) are missing, use this javaLogHelper's properties
	   */
	  public JavaLogHelper(String name) {
		    
		  /*
		  if ( (System.getProperty("java.util.logging.config.file")!=null) & !(System.getProperty("java.util.logging.config.file").equals("")) )
		    {
		    	properties_file = System.getProperty("java.util.logging.config.file");
		    }
		  */
		    this.configure(properties_file);
	
		    javaLogger = java.util.logging.Logger.getLogger(name);
		  }

	  /*
	 * @param name
	 * @param properties_file
	 */
	public JavaLogHelper(String name, String properties_file){
		  	this.properties_file = properties_file;
		    this.configure(this.properties_file);
		    javaLogger = java.util.logging.Logger.getLogger(name);
	  }

	  // Default no-argument constructor
	  public JavaLogHelper() {
		  this("JavaLogHelper");
	  }
	  
	  /**
	   * Obtain a new or existing Logger instance. 
	   */
	  public static JavaLogHelper getLogger(String name) {
	    return new JavaLogHelper(name);
	  }
	  
	  public String propertiesfile() {
		  return this.properties_file ;
	  }

	  private static void configure(String filename) {
	    FileInputStream is = null; 
	    try {
	      is = new FileInputStream(filename);
	      LogManager.getLogManager().readConfiguration(is);
	    }
	    catch (Exception e) {
	      System.err.println("JavaLogHelper-ERROR: Failed to initialize logging system due to following error: " + e.getMessage());
	      e.printStackTrace();
	    }
	    finally {
	      try {
	        is.close();
	      } catch (IOException e) {}
	    }
	  }
	  
	  
	  public void info(Object o)
	  {
	    this.javaLogger.logp( Level.INFO, "com.jpmc.wss.im.util.JavaLogHelper", "info", o.toString() );
	  }

	  public void info(Object o, Exception e)
	  {
		this.javaLogger.logp( Level.INFO, "com.jpmc.wss.im.util.JavaLogHelper", "info", o.toString() + e.toString() );
	  }
	  
	  public void info(String sourceClass, String sourceMethod, String message) {
	    javaLogger.logp(Level.INFO, sourceClass, sourceMethod, message);
	  }
	  
	  public void info(String sourceClass, String sourceMethod, String message, Throwable thrown) {
	    javaLogger.logp(Level.INFO, sourceClass, sourceMethod, message, thrown);
	  }
	  
	  public void debug(String sourceClass, String sourceMethod, String message) {
	    javaLogger.logp(Level.FINE, sourceClass, sourceMethod, message);
	  }
	  
	  public void debug(String sourceClass, String sourceMethod, String message, Throwable thrown) {
	    javaLogger.logp(Level.FINE, sourceClass, sourceMethod, message, thrown);
	  }

	  public void trace(String sourceClass, String sourceMethod, String message) {
	    javaLogger.logp(Level.FINER, sourceClass, sourceMethod, message);
	  }
	  
	  public void trace(String sourceClass, String sourceMethod, String message, Throwable thrown) {
	    javaLogger.logp(Level.FINER, sourceClass, sourceMethod, message, thrown);
	  }
	  
	  public void warn(Object o)
	  {
		    this.javaLogger.logp( Level.INFO, "com.jpmc.wss.im.util.JavaLogHelper", "info", o.toString() );		  
	  }
	  
	  public void warn(String sourceClass, String sourceMethod, String message) {
	    javaLogger.logp(Level.WARNING, sourceClass, sourceMethod, message);
	  }
	  
	  public void warn(String sourceClass, String sourceMethod, String message, Throwable thrown) {
	    javaLogger.logp(Level.WARNING, sourceClass, sourceMethod, message, thrown);
	  }

	  public void error(String sourceClass, String sourceMethod, String message) {
	    javaLogger.logp(Level.SEVERE, sourceClass, sourceMethod, message);
	  }

	  public void error(String sourceClass, String sourceMethod, String message, Throwable thrown) {
	    javaLogger.logp(Level.SEVERE, sourceClass, sourceMethod, message, thrown);
	  }
	  
	  public boolean isTraceEnabled() {
	    return javaLogger.isLoggable(Level.FINER);
	  }

	  public boolean isDebugEnabled() {
	    return javaLogger.isLoggable(Level.FINE);
	  }
	  
	  public void enableDebug() {
	    javaLogger.setLevel(Level.FINE);
	  }

	  public void disableDebug() {
	    javaLogger.setLevel(Level.INFO);
	  }
	
}

package com.example.collection;

import java.util.*;



public class JavaCollectionDemo {

	/**
	 * @param <E>
	 * @param args
	 */
	public static void main(String[] args) {
		
		//Array of Object
//	     Object[] STAcctList = tree.getAllStandAloneAccounts();
//	     for(int i =0; i<STAcctList.length; i++){
//	    	 AccountEntry ae = (AccountEntry)STAcctList[i] ; 
//	    	    SCRLogger.log(SCRLogger.INFO,  "Stand Alone Account: " + ae.getAltId()  + " - "	+ ae.getId() );
//	   	}
//		
		// << SET >>
		// HashSet
		HashSet<String> hs = new HashSet<String>();
		hs.add("a");
		
		// TreeSet
		TreeSet<String> ts = new TreeSet<String>();
		ts.add("a");
		
		// LinkedHashSet
		LinkedHashSet<String> lhs = new LinkedHashSet<String>();
		lhs.add("a");
		

		// <<LIST>>

		// Array List
		ArrayList<String> al = new ArrayList<String>();
		al.add(0, "william");
		al.add(1, "Jenny");
	
		// Vector 
		Vector<String>  v = new Vector<String>();
		v.add(0, "tim");
		v.add(1,"mathhew");
		
		// LinkedList
		LinkedList<String> ll = new LinkedList<String>();
		ll.add("a");
		ll.add("b");
		

		
		////////////////// <<TreeMap>> ////////////////////////
		TreeMap<String, String> tm = new TreeMap<String, String>();
		tm.put("William Deng", "668-1238");
		tm.put("Jenny Deng", "916-3039");

		// << Queue >>

		// PriorityQueue

		// ConcurrentLinkedQueue

		// LinkedBlockingQueue


		// <<Deque >>

		// ArrayDeque

		// LinkedBlockingDeque


	}

}

package com.example.system.command;

import java.io.IOException;
import java.io.InputStream;

public class ExecuteSystemCommand {
	public static void main(String[] args) throws IOException, InterruptedException {
		// you need a shell to execute a command pipeline
		/*
		 * List<String> commands = new ArrayList<String>();
		 * commands.add("/bin/sh"); commands.add("-c"); commands.add(
		 * "ls -l /var/tmp | grep foo");
		 * 
		 * SystemCommandExecutor commandExecutor = new
		 * SystemCommandExecutor(commands); int result =
		 * commandExecutor.executeCommand();
		 * 
		 * StringBuilder stdout =
		 * commandExecutor.getStandardOutputFromCommand(); StringBuilder stderr
		 * = commandExecutor.getStandardErrorFromCommand();j
		 * 
		 * System.out.println("STDOUT"); System.out.println(stdout);
		 * 
		 * System.out.println("STDERR"); System.out.println(stderr);
		 */

		// String[] cmd = { "cd /WordFrequency/", "ls -l | grep record" };
		// String[] cmd = { "cd /WordFrequency", "ls -l | grep record" };

		String[] cmd = { "dir", "dir" };

		try {

			// Execute the command and wait for it to complete
			Process child = Runtime.getRuntime().exec("dir");
			child.waitFor();

			// Print the first 16 bytes of its output
			InputStream i = child.getInputStream();
			byte[] b = new byte[16];
			i.read(b, 0, b.length);
			System.out.println(new String(b));

		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}

	}
}
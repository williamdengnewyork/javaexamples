import java.io.BufferedReader;
import java.io.File;
import java.util.Arrays;
import java.util.Scanner;


/*   example input. 4 - signal there are 4 lines below to load
4
add hack
add hackerrank
find hac
find hak

2
3 2 4 5
9 7 6 8
 */

public class HackerRank_ScannerInput {

	private static Scanner in;
	
	public static void main(String[] args) {

		in = new Scanner(System.in);
		
		//oneNumberAndLines();
		oneNumberAndArrays();

	}

	private static void oneNumberAndArrays() {
		int n = in.nextInt(); // fisrt line number =  num of input elements in second line

		int[] input = new int[n];		
	    for (int i = 0; i < n ; i++)
	    {
		    input[i] = in.nextInt();
	    }	    
		System.out.println("->" + Arrays.toString(input));		
			
	}

	private static void oneNumberAndLines() {
		int n = in.nextInt(); // read num of input elements
		
		for (int i = 0; i < n + 1; i++) {
			String instruction = in.nextLine();  // read next line as String
			System.out.println("->" + instruction);
		}
		
	}

	
	
}

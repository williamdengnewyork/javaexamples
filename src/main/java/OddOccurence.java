class OddOccurence {

	public static int solution(int[] A) {
        // write your code in Java SE 8
        if (A.length==0){
        	return 0;
        }
        if (A.length==1){
        	return A[0];
        }
		
		int ans = 0;
        //int bef = 0;
        	
        for (int i=0; i<A.length; i++){
        	//bef = ans;
        	ans = ans^A[i];
        	//System.out.println(bef + " ^" + A[i] + "= " + ans);
        }
		return ans;
    }
	
	public static void main(String[] args) {
//		int[] A = {2,3,3,2,7, 6, 6};
		int[] A = {2,2};
		
		System.out.println(solution(A));
	}
	
}

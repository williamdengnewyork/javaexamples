import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//JPMC Wayne team, HireVue, 2/26/2017
public class HireVueJPMCWayne_FindLongestSuffixString {

	public static int getLongestCommonSubstring(String a, String b) {

		int al = a.length();
		int bl = b.length();

		int max = 0;

		int[][] dp = new int[al][bl];

		for (int i = 0; i < al; i++) {
			for (int j = 0; j < bl; j++) {
				if (a.charAt(i) == b.charAt(j)) {
					if (i == 0 || j == 0) {
						dp[i][j] = 1;
					} else {
						dp[i][j] = dp[i - 1][j - 1] + 1;
					}

					if (max < dp[i][j])
						max = dp[i][j];
				}

			}
		}

		return max;
	}

	public static String longestSubstring(String str1, String str2) {

		StringBuilder sb = new StringBuilder();
		if (str1 == null || str1.isEmpty() || str2 == null || str2.isEmpty())
			return "";

		// ignore case
		str1 = str1.toLowerCase();
		str2 = str2.toLowerCase();

		// java initializes them already with 0
		int[][] num = new int[str1.length()][str2.length()];
		int maxlen = 0;
		int lastSubsBegin = 0;

		for (int i = 0; i < str1.length(); i++) {
			for (int j = 0; j < str2.length(); j++) {
				if (str1.charAt(i) == str2.charAt(j)) {
					if ((i == 0) || (j == 0))
						num[i][j] = 1;
					else
						num[i][j] = 1 + num[i - 1][j - 1];

					if (num[i][j] > maxlen) {
						maxlen = num[i][j];
						// generate substring from str1 => i
						int thisSubsBegin = i - num[i][j] + 1;
						if (lastSubsBegin == thisSubsBegin) {
							// if the current LCS is the same as the last time
							// this block ran
							sb.append(str1.charAt(i));
						} else {
							// this block resets the string builder if a
							// different LCS is found
							lastSubsBegin = thisSubsBegin;
							sb = new StringBuilder();
							sb.append(str1.substring(lastSubsBegin, i + 1));
						}
					}
				}
			}
		}

		return sb.toString();
	}

	// find "suffix string" --- start from the end !!!
	public static String longestSuffixString(String str1, String str2) {
		
		StringBuilder sb = new StringBuilder();
		if (str1 == null || str1.isEmpty() || str2 == null || str2.isEmpty())
			return "NULL";

		// for reversion search
		for (int j = str2.length() - 1, i = str1.length() - 1; j >= 0 && i >= 0; j--, i--) {
			if (str1.charAt(i) == str2.charAt(j)) {
				sb.append(str1.charAt(i));
			} else {
				break;
			}
		}

		String result = sb.reverse().toString();
		if (result == null || result.equals("")) {
			result = "NULL";
		}

		return result;
	}

	public static void main(String[] args) throws IOException {

		// Test Cases
		// Cornfield, asdoutfield
		// Manhours, manhole
		// asdf okd, df okd

		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String s;
		while ((s = in.readLine()) != null) {
			System.out.println(s);
			//**** Need to handle edge case if there's no "," 
			String firsts = s.substring(0, s.indexOf(",")).trim();
			String seconds = s.substring(s.indexOf(",") + 1, s.length()).trim();
			// System.out.println(firsts);
			// System.out.println(seconds);
			System.out.println(HireVueJPMCWayne_FindLongestSuffixString.longestSuffixString(firsts, seconds));
		}

		// System.out.println(HireVueJPMCWayne_FindLongestSuffixString.getLongestCommonSubstring("Cornfield",
		// "outfield"));
		// System.out.println(HireVueJPMCWayne_FindLongestSuffixString.longestSubstring("Cornfield",
		// "outfield"));
		// System.out.println(HireVueJPMCWayne_FindLongestSuffixString.longestSubstring("Manhours",
		// "manhole"));

	}

}

import java.util.Set;

public class CityNode {
		public String cityName;		
		public Set<CityNode> connections;
		public boolean visit = false;

		//contructor
		CityNode(String name, String connection){
			this.cityName = name;
			this.connections.add(new CityNode(connection));			
		}

		//contructor 2
		CityNode(String name){
			this.cityName = name;
		}		
		
		public String getCityName() {
			return cityName;
		}

		public void setCityName(String cityName) {
			this.cityName = cityName;
		}


		public Set<CityNode> getConnections() {
			return connections;
		}


		public void setConnections(Set<CityNode> connections) {
			this.connections = connections;
		}
		
		
	}

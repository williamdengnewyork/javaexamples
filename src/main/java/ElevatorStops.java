import java.util.*;


// Codility Test from BOA -- 2/27/2017

// Walk through People Array 
// Create one trip - Add up a) total people count, b) total weight, c)Floor list 
// If a) or b) reaches limit, this is one trip. Tripstops= unique floor + 1 (go back to lobby)
// Final: TotalStops = totalStops + tripStop

public class ElevatorStops {

	public int solution(int[] A, int[] B, int M, int X, int Y) {
		// int M=3 floor, X=5, Y=200 -weight;
		int totalStops = 0;
		long totalWeightPerTrip = 0;
		int totalPersonsPerTrip = 0;
		List<Integer> floorList = new ArrayList<Integer>();

		for (int i = 0; i < A.length; i++) {
			if (((totalPersonsPerTrip + 1) <= X) && ((totalWeightPerTrip + A[i]) <= Y)) { // test before go !
				totalPersonsPerTrip++;
				totalWeightPerTrip += A[i];
				floorList.add(B[i]);
				if (i==A.length-1){
					totalStops = totalStops + (new TreeSet(floorList)).size() + 1;
				}else{
				    continue;
				}
			} else { 
				// elevator full, go for a trip
				totalStops = totalStops + (new TreeSet(floorList)).size() + 1;
				//System.out.println("this trip - " + totalStops);
				totalPersonsPerTrip = 0;
				totalWeightPerTrip = 0;
				floorList.clear();
				i--;    // roll back counter
			}
		}

		//totalStops = totalStops + (new TreeSet(floorList)).size() + 1;
		
		return totalStops;
	}

	public static void main(String[] args) {
		ElevatorStops s = new ElevatorStops();

		int[] A = { 40, 40, 100, 80, 20 };
		int[] B = { 3, 3, 2, 2, 3 };
		int M = 3, X = 5, Y = 200;
			
//		int[] A = { 60, 80, 40 };
//		int[] B = { 2,3,5 };
//		int M = 5, X = 2, Y = 200;

		System.out.println("Total Stops = " + s.solution(A, B, M, X, Y));

	}

}

/*
Example test:   ([60, 80, 40], [2, 3, 5], 5, 2, 200)
OK

Example test:   ([40, 40, 100, 80, 20], [3, 3, 2, 2, 3], 3, 5, 200)
OK
*/

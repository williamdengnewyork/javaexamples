import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

// **  Failed many tests

// Hacker Rank test
// Validate Well format braces String. e.g.  {"{","[", "(",")", "]", "}"};
public class Solution_BraceValidationWithStack {

	static int braces(String[] values) {

		int accum = 0;		
		// use stack's push, peek, pop to maintain
		Stack<Integer> st = new Stack<Integer>(); 
		
		for (int i = 0; i < values.length; i++) {
			String s = values[i];
			int s_value = getValue(s);

			if(i==0 && s_value<0){
				return 1; //bad - first is -				
			}
			
			if(i>0 && s_value<0){
				//System.out.println(st.peek());
			if (s_value != -(Integer)st.peek()) {
					return 1; //bad - 
				}else
			st.pop();
			}			
			accum += s_value;
			if (s_value>0){
				st.push(s_value);
			}
		}
		
		if (accum==0)  return 0; //good		
		return 0;  // Yes
	}
	
	// assign value to each brace
	static int getValue(String s){
		if (s.equals("{")) return 3;
		if (s.equals("}")) return -3;
		if (s.equals("[")) return 2;
		if (s.equals("]")) return -2;
		if (s.equals("(")) return 1;
		if (s.equals(")")) return -1;
		
		return 0;
	}

	public static void main(String[] args) throws IOException {
		//String[] input = {"{","[", "(",")", "]", "}"};
		//String[] input = {"{","[", "[","]", "]", ")"};
		//int ans = braces(input);
		int ans=-1;
		
		// take inputs
		Scanner in = new Scanner(System.in);		
		int num = in.nextInt();

		for (int i = 0; i <= num; i++) {
			String line = in.nextLine();			
			if (line.length()>0){
			//System.out.println(line.length());
			String[] a_str = line.trim().split("");
			//System.out.println("i= " + i + Arrays.toString(a_str));
			if (a_str.length>0){
				
			ans = braces(a_str);
			if (ans==0)
				System.out.println("YES");
			else
				System.out.println("NO");
			}
			}
		}

		
		
	}

}

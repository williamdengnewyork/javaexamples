
public class BitwiseOpreations {


	   public static void main(String args[]) {
	      int a = 60;	/* 60 = 0011 1100 */
	      int b = 13;	/* 13 = 0000 1101 */
	      int c = 0;
	      c = a & b;        /* 12 = 0000 1100 */

	      System.out.println(" Bitwise " );

	      System.out.println("AND: " + a + " & " + b + " = " + (a & b) );

	      c = a | b;        /* 61 = 0011 1101 */
	      System.out.println("OR : " + a + " | " + b + " = " + (a | b) );

	      /*
	      XOR operation properties:
	    	  1) XOR of any number n with itself gives 0, i.e., n ^ n = 0
	    	  2) XOR of any number n with 0 gives n, i.e., n ^ 0 = n
	    	  3) XOR is cumulative and associative. A^B=B^A and (A^B)^C=A^(B^C)
		  usage:
		  1. Swapping value of x and y
		    x = x ^ y ;
  			y = x ^ y ;
  			x = x ^ y ;
		  2. Find odd occurence of number out of many pairs
		  https://devenbhooshan.wordpress.com/2012/07/08/xor-properties/
		  */
	      c = a ^ b;        /* 49 = 0011 0001 */
	      System.out.println("XOR: " + a + " ^ " + b + " = " + (a ^ b) );

	      c = ~a;           /*-61 = 1100 0011 */
	      System.out.println("~a = " + c );

	      c = a << 2;       /* 240 = 1111 0000 */
	      System.out.println("a << 2 = " + c );

	      c = a >> 2;       /* 15 = 1111 */
	      System.out.println("a >> 2  = " + c );

	      c = a >>> 2;      /* 15 = 0000 1111 */
	      System.out.println("a >>> 2 = " + c );
	   }
	}
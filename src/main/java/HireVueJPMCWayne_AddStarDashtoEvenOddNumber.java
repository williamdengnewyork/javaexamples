import java.io.*;

// JPMC Wayne team, HireVue, 2/26/2017
public class HireVueJPMCWayne_AddStarDashtoEvenOddNumber {

	public static String decorString(String str1) {
		StringBuilder sb = new StringBuilder();
		if (str1 == null || str1.isEmpty())
			return "";

		int pre_num = 0; // nor even nor odd
		int cur_num = 0;

		for (int i = 0; i < str1.length(); i++) {
			cur_num = Character.getNumericValue(str1.charAt(i));
			// System.out.println(cur_num);
			if (pre_num != 0 && cur_num != 0) {
				if ((cur_num % 2 == 0) && (pre_num % 2 == 0)) {
					sb.append("*");
				}
				if ((cur_num % 2 == 1) && (pre_num % 2 == 1)) {
					sb.append("-");
				}
			}
			sb.append(str1.charAt(i));
			pre_num = Character.getNumericValue(str1.charAt(i));
		}

		String result = sb.toString();
		if (result == null || result.equals("")) {
			result = "";
		}

		return result;

	}

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String s;

		// s="12345678";// --> 12467930 12*4*67-9-30 ;//"12345678" -->
		// "12345678";
		// System.out.println(HireVueJPMCWayne_AddStarDashtoEvenOddNumber.decorString(s));

		while ((s = in.readLine()) != null) {
			// System.out.println(s);
			System.out.println(HireVueJPMCWayne_AddStarDashtoEvenOddNumber.decorString(s));
		}
	}

}

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class ShiftArray {
/*
3 2
344 210 102

--> 102 344 210 
 */	
	public static void main(String[] args) {
		
		// take inputs
		Scanner in = new Scanner(System.in);		
		int n = in.nextInt();
		int d = in.nextInt();
		int[] a = new int[n];
		int[] o = new int[n];
		for (int i = 0; i < n; i++) {
			o[i] = in.nextInt();
		}
		
		// proc
		for (int a_i = 0; a_i < o.length; a_i++) {
			int pos = a_i + d;
			if (pos >= o.length){
				pos = pos - o.length;
			}
			a[a_i] = o[pos];
		}
		for ( int k=0; k < a.length; k++){
			System.out.print(a[k] + " ");
		}
		
	}
}

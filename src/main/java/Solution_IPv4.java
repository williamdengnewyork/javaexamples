import java.util.Scanner;

public class Solution_IPv4{


	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		while ( in.hasNextLine()) {
			
			String iP = in.nextLine();
			
			MyRegex mr = new MyRegex(iP);
			
			System.out.println(mr.isMatch());
		}

		in.close();
	}

}
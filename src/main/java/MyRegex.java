public class MyRegex extends Solution {
	private static String pattern = "((25[0-5])|(2[0-4][0-9])|([0-1]{0,1}[0-9]{1,2}))"; //"/* Write your RegEx here. */";
	private String iP = null; 
	
	public MyRegex(String iP) {
		this.iP=iP;
	}

	
	public boolean isMatch() {
		String regex = String.format("%s\\.%s\\.%s\\.%s", pattern, pattern, pattern,	pattern);
		return this.iP.matches(regex);
	}
	

}

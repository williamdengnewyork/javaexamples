
import java.util.*;

public class Compares {
	public static void main(String args[]) {
		String[] cities = { "Bangalore", "Pune", "San Francisco", "New York City" };
		MySort ms = new MySort();

		Arrays.sort(cities, ms);
		System.out.println(Arrays.binarySearch(cities, "New York City"));
	}

	static class MySort implements Comparator {

// HackRank Test - read code and predict output   ==> cause compile error		
//		@Override
//		public int compare(String a, String b) {  /// ==> cause compile error
//			return b.compareTo(a);
//		}

		public int compare(Object o1, Object o2) {
			return ((String) o1).compareTo((String)o2);
		}

		
	}
}
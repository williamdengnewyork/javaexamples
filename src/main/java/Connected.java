import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

// City Connection Search using Graph
public class Connected {

	public static void main(String[] args) {

		// 0. take String[] args: 0-filename, 1-city1, 2-city2
		if (args.length<3){
			System.out.println("Not enough arguments to process! e.g. java Connected<filename> <cityname1> <cityname2>.   Exit:-1");
			System.exit(-1);
		}		
		String CityFileName = args[0];
		String searchCity1 = args[1];
		String searchCity2 = args[2];
		
		// 1. Validate File, 1-searchCity1, 2-searchCity2
		File datafile = new java.io.File(args[0]);
		if (!datafile.isFile()){
			System.out.println(args[0] + " is not valid data file! Exit:-2");			 
			System.exit(-2);
		}
		if (searchCity1.trim().equals("") || searchCity1==null){
			System.out.println(args[1] + " cityname1 is empty! Exit:-3");			 
			System.exit(-3);
		}
		if (searchCity2.trim().equals("") || searchCity2==null){
			System.out.println(args[1] + " cityname2 is empty! Exit:-4");			 
			System.exit(-4);
		}
		
		// 2. Read File and load into Graph
		System.out.println("RecordsFile = " + Paths.get(CityFileName));
   	    Graph travelGraph = new Graph();
		try {
			for (String record : java.nio.file.Files.readAllLines(Paths.get(CityFileName), StandardCharsets.UTF_8)) {
				String[] citypair = record.split(",");
				if (citypair.length==2){
					// 3. load each line to graph node
					travelGraph.addCityNodeConnection(citypair[0], citypair[1]);					
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		// 4. search connection between city1 and city2
		boolean result = travelGraph.isConnected(searchCity1, searchCity2) ;		
		if (result)
			System.out.println("yes");
		else
			System.out.println("no");
		
		System.exit(0);
	}

}

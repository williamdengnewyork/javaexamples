import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Graph {
	Map<String, CityNode> cityNodeMap = new HashMap<String, CityNode>();;

	public void addCityNodeConnection(String city1, String city2) {
		// add city1
		cityNodeMap.put(city1, new CityNode(city1, city2));
		// add city2
		cityNodeMap.put(city2, new CityNode(city2, city1));
		return;
	}

	public boolean isConnected(String city1, String city2) {
			boolean ans = false;

			if (cityNodeMap.get(city1)==null){
				System.out.println("No Connection for city "+ city1);
				return false;
			}

			if (cityNodeMap.get(city2)==null){
				System.out.println("No Connection for city "+ city2);
				return false;
			}
			// begin with city1, search for city2
			Set<CityNode> conn = cityNodeMap.get(city1).getConnections();			
			ans =inConnection(conn, city2);
			return ans;
	}

	
	public boolean inConnection(Set<CityNode> conn, String city2){
		boolean ans = false;
		for (CityNode cnode : conn){
			if (cnode.getCityName().equals(city2)){
				return true;
			}else{
				// recursive search here !
				if (inConnection(cnode.getConnections(), city2)){ 
					return true;
				}
			}
		}
		return ans;
	}

		
}

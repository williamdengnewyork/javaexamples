import java.util.stream.IntStream;

/**
 * @author Lenovo
 * Jan 15, 2017
 */

/**
 * @author Lenovo
 *
 */
public class Stream {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] a = { 1, 2, 3, 4 };
		boolean contains = IntStream.of(a).anyMatch(x -> x == 5);
		System.out.println(contains);
	}

}

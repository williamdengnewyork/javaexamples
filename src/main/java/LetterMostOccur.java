import java.util.*;


// Codility BOA Test  2/26/2017

// bug fix
// 1. if more than one occur most frquency, return the last letter (odd), instead of first
// 2. edge case letter "a"
public class LetterMostOccur {
	String solution(String S) {
        int[] occurrences = new int[26];
                    
        for (char ch : S.toCharArray()) {
            occurrences[ch - 'a']++;
        }

        char best_char = 'a';
        int  best_res  = 0;

        for (int i = 0; i < 26; i++) {    // was i=1, array start 0 for  2. edge case letter "a"
            if (occurrences[i] > best_res) {   // *** was >= 1. take the  first most
                best_char = (char)((int)'a' + i);
                best_res  = occurrences[i];
            }
        }

        return Character.toString(best_char);
    }

    
    public static void main(String[] args) {
    	LetterMostOccur m = new LetterMostOccur();    	
    	String s="abcdefghijklmnozpqrsztuvwbxyz";
    	// wrong =  - o,  -l aaaabcde -b
    	//correct = hheellooo, hhheelloo  hheelloo hhheellloo    	
    	System.out.println(s + " - " + m.solution(s));

	}
}



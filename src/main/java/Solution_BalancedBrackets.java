import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

// Working !
// Hacker Rank test
// Validate Well format braces String. e.g.  {"{","[", "(",")", "]", "}"};
// Codility - Stack+Queue/Bracket

// Idea:
// 1. convert input to char[]. line.toCharArray()
// 2. Walk through char[], if char is "starter bracket/{[(", push its opposite to "Stack"
// 3. if not "starter bracket/{[(", and match to Stack.peek(), then this char is good format. Otherwise, bad.
// 4. Stack.pop(), move on to next char, do 2,3,4
// Edge: 1.first char is ending bracket.  2. stack empty after last pop() 

public class Solution_BalancedBrackets {

	static String[] braces(String[] values) {

		String[] output = new String[values.length];

		// if (values.length() <= 1) {
		// return false;
		// }
		String line = new String();

		Stack<Character> st = new Stack<Character>();

		for (int i = 0; i < values.length; i++) {
			line = values[i];

			for (char bracket : line.toCharArray()) {
				if (bracket == '{')
					st.push('}');
				else if (bracket == '[')
					st.push(']');
				else if (bracket == '(')
					st.push(')');
				else {
					if (st.empty() || bracket != st.peek()) { // may be empty
																// after last
																// pop(),
						// return false;
						output[i] = "NO";
						st.pop();
					} else {
						output[i] = "YES";
						st.pop();
					}
				}

			}

		}
		return output;
	}

	public static void main(String[] args) throws IOException {

		// take user inputs
		// Not working in Eclpse Console 
		/*
		Scanner in = new Scanner(System.in);
		int num = in.nextInt();

		String[] _values = new String[num];
		String line = new String();
		for (int i = 0; i <= num; i++) {
			try {
				line = in.nextLine();
			} catch (Exception e) {
				line = null;
			}
			_values[i] = line;
		}
		*/

		String[] _values = {"{","[", "(",")", "]", "}"};
		System.out.println("output:");
		String[] res = braces(_values);
		for (int res_i = 0; res_i < _values.length; res_i++) {
			System.out.println(String.valueOf(res[res_i]));
		}

	}

}
